package controller;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import model.TipoPagamento;
import utils.Util;

public class TipoPagamentoDao {

    private final ConexaoDao conexaoDao;

    public TipoPagamentoDao() {
        this.conexaoDao = new ConexaoDao();
    }

    public ArrayList<TipoPagamento> getListaTipoPagamento() throws Exception {
        try {
            ArrayList<TipoPagamento> listaTipoPagamento = new ArrayList<>();
            
            conexaoDao.conectar();
            
            PreparedStatement stmt = conexaoDao.getConnection().prepareStatement("SELECT * FROM tipo_pagamento");
            ResultSet result = stmt.executeQuery();
            
            while(result.next()) {
                TipoPagamento tipoPagamento = new TipoPagamento();
                tipoPagamento.setIdTipo(result.getInt("idtipo"));
                tipoPagamento.setDescricao(result.getString("descricao"));
                listaTipoPagamento.add(tipoPagamento);
            }
            
            conexaoDao.desconectar();
            
            return listaTipoPagamento;
        } catch(Exception e) {
            Util.abreDialogErro("Erro", e.getMessage());
            conexaoDao.desconectar();
            throw e;
        } 
    }
}
