package model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProdutoVenda {

    private int codigoProduto, quantidade;
    private double precoProduto;
}
