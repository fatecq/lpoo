package utils;

import classes.Tipo_Imovel;
import java.awt.Image;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

public class Util {
    
    public static ArrayList<Tipo_Imovel> listaTipoImovel = new ArrayList<>();
    
    public static void abreDialogInformacao(String titulo, String mensagem) {
        JOptionPane.showMessageDialog(null, mensagem, titulo, JOptionPane.INFORMATION_MESSAGE);
    }
    
    public static void abreDialogErro(String titulo, String mensagem) {
        JOptionPane.showMessageDialog(null, mensagem, titulo, JOptionPane.ERROR_MESSAGE);
    }
  
    public static void abreDialogAtencao(String titulo, String mensagem) {
        JOptionPane.showMessageDialog(null, mensagem, titulo, JOptionPane.WARNING_MESSAGE);
    }
    
    //Define o ícone do frame
    public static Image setProjectIconImage(Class classe) {
        return new ImageIcon(classe.getResource(Constantes.CAMINHO_IMAGEM)).getImage();
    }
}
