package view;

import utils.Util;

public class TelaPrincipal extends javax.swing.JFrame {

    private TelaCadastroVenda telaCadastroVenda;
    private TelaCadastroProduto telaCadastroProduto;
    private TelaConsultaProduto telaConsultaProduto;
    private TelaRelatorio telaRelatorio;

    public TelaPrincipal() {
        initComponents();
        lbTitulo.setText("Seja bem vindo(a), " + Util.nomeFuncionario);

        telaCadastroVenda = new TelaCadastroVenda();
        telaCadastroProduto = new TelaCadastroProduto();
        telaRelatorio = new TelaRelatorio();
        telaConsultaProduto = new TelaConsultaProduto();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnSair = new javax.swing.JButton();
        lbTitulo = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        menuCadVenda = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        menuCadProduto = new javax.swing.JMenuItem();
        jMenu4 = new javax.swing.JMenu();
        menuConsultaProdutos = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JPopupMenu.Separator();
        menuRelatorio = new javax.swing.JMenuItem();
        menuAjuda = new javax.swing.JMenu();
        menuSobre = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        menuSair = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Papelaria");
        setIconImage(Util.setProjectIconImage(this.getClass())
        );
        setResizable(false);

        btnSair.setText("SAIR");
        btnSair.setFocusPainted(false);
        btnSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSairActionPerformed(evt);
            }
        });

        lbTitulo.setFont(new java.awt.Font("Cantarell", 3, 14)); // NOI18N
        lbTitulo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbTitulo.setText("Seja bem vindo(a)");

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/papelaria.png"))); // NOI18N

        jMenu1.setText("Cadastros");

        menuCadVenda.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/venda.png"))); // NOI18N
        menuCadVenda.setText("Cadastrar venda");
        menuCadVenda.setPreferredSize(new java.awt.Dimension(181, 41));
        menuCadVenda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCadVendaActionPerformed(evt);
            }
        });
        jMenu1.add(menuCadVenda);
        jMenu1.add(jSeparator2);

        menuCadProduto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/produtos.png"))); // NOI18N
        menuCadProduto.setText("Cadastrar produto");
        menuCadProduto.setPreferredSize(new java.awt.Dimension(181, 41));
        menuCadProduto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCadProdutoActionPerformed(evt);
            }
        });
        jMenu1.add(menuCadProduto);

        jMenuBar1.add(jMenu1);

        jMenu4.setText("Consultas");

        menuConsultaProdutos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/monitorar_produtos.png"))); // NOI18N
        menuConsultaProdutos.setText("Consultar produtos");
        menuConsultaProdutos.setPreferredSize(new java.awt.Dimension(200, 41));
        menuConsultaProdutos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuConsultaProdutosActionPerformed(evt);
            }
        });
        jMenu4.add(menuConsultaProdutos);
        jMenu4.add(jSeparator3);

        menuRelatorio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/listar.png"))); // NOI18N
        menuRelatorio.setText("Relatório de vendas");
        menuRelatorio.setPreferredSize(new java.awt.Dimension(200, 41));
        menuRelatorio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuRelatorioActionPerformed(evt);
            }
        });
        jMenu4.add(menuRelatorio);

        jMenuBar1.add(jMenu4);

        menuAjuda.setText("Ajuda");

        menuSobre.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/versao.png"))); // NOI18N
        menuSobre.setText("Sobre");
        menuSobre.setPreferredSize(new java.awt.Dimension(90, 41));
        menuSobre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSobreActionPerformed(evt);
            }
        });
        menuAjuda.add(menuSobre);
        menuAjuda.add(jSeparator1);

        menuSair.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/fechar.png"))); // NOI18N
        menuSair.setText("Sair");
        menuSair.setPreferredSize(new java.awt.Dimension(90, 41));
        menuSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSairActionPerformed(evt);
            }
        });
        menuAjuda.add(menuSair);

        jMenuBar1.add(menuAjuda);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(144, 144, 144)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnSair, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addContainerGap(140, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbTitulo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addComponent(jLabel2)
                .addGap(7, 7, 7)
                .addComponent(lbTitulo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 67, Short.MAX_VALUE)
                .addComponent(btnSair, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(42, 42, 42))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSairActionPerformed
        Util.encerrarPrograma();
    }//GEN-LAST:event_btnSairActionPerformed

    private void menuCadVendaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCadVendaActionPerformed
        if (!telaCadastroVenda.isVisible()) {
            telaCadastroVenda = new TelaCadastroVenda();
            telaCadastroVenda.setVisible(true);
        }
    }//GEN-LAST:event_menuCadVendaActionPerformed

    private void menuSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSairActionPerformed
        Util.encerrarPrograma();
    }//GEN-LAST:event_menuSairActionPerformed

    private void menuSobreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSobreActionPerformed
        Util.abreDialogInformacao("Sobre", "Prova de POO - Gabriel Quilice");
    }//GEN-LAST:event_menuSobreActionPerformed

    private void menuRelatorioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuRelatorioActionPerformed
        if (!telaRelatorio.isVisible()) {
            telaRelatorio = new TelaRelatorio();
            telaRelatorio.setVisible(true);
        }
    }//GEN-LAST:event_menuRelatorioActionPerformed

    private void menuConsultaProdutosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuConsultaProdutosActionPerformed
        if (!telaConsultaProduto.isVisible()) {
            telaConsultaProduto = new TelaConsultaProduto();
            telaConsultaProduto.setVisible(true);
        }
    }//GEN-LAST:event_menuConsultaProdutosActionPerformed

    private void menuCadProdutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCadProdutoActionPerformed
        if (!telaCadastroProduto.isVisible()) {
            telaCadastroProduto = new TelaCadastroProduto();
            telaCadastroProduto.setVisible(true);
        }
    }//GEN-LAST:event_menuCadProdutoActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TelaPrincipal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnSair;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JPopupMenu.Separator jSeparator3;
    private javax.swing.JLabel lbTitulo;
    private javax.swing.JMenu menuAjuda;
    private javax.swing.JMenuItem menuCadProduto;
    private javax.swing.JMenuItem menuCadVenda;
    private javax.swing.JMenuItem menuConsultaProdutos;
    private javax.swing.JMenuItem menuRelatorio;
    private javax.swing.JMenuItem menuSair;
    private javax.swing.JMenuItem menuSobre;
    // End of variables declaration//GEN-END:variables
}
