package utils;

import classes.Tipo_Imovel;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class Metodos {

    private Tipo_Imovel tipoImovel;

    public Metodos() {
    }

    public Metodos(Tipo_Imovel tipoImovel) {
        this.tipoImovel = tipoImovel;
    }

    public void encerrarPrograma() {
        int escolha = JOptionPane.showConfirmDialog(null, "Deseja sair e encerrar o programa?", "Sair",
                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

        if (escolha == JOptionPane.YES_OPTION) {
            System.exit(0);
        }
    }

    public void salvar() {
        Util.listaTipoImovel.add(tipoImovel);
    }

    public void alterar(int index) {
        Util.listaTipoImovel.set(index, tipoImovel);
    }

    public void excluir(int codigoImovel) {
        Util.listaTipoImovel.removeIf(tipoImovel -> tipoImovel.getCodigoImovel() == codigoImovel);
    }

    public ArrayList<Tipo_Imovel> buscar(String pesquisa) {
        ArrayList<Tipo_Imovel> imoveis = new ArrayList<>();

        if (pesquisa.matches("[0-9]+")) {
            Util.listaTipoImovel.forEach(tipoImovel -> {
                if (tipoImovel.getCodigoImovel() == Integer.parseInt(pesquisa)) {
                    imoveis.add(tipoImovel);
                }
            });
            
        } else {
            Util.listaTipoImovel.forEach(tipoImovel -> {
                if (tipoImovel.getDescricao().contains(pesquisa)) {
                    imoveis.add(tipoImovel);
                }
            });
        }

        return imoveis;
    }

    public double getTipoImovelValorFinal(int codigoImovel) {

        for (Tipo_Imovel tipoImovel : Util.listaTipoImovel) {
            if (tipoImovel.getCodigoImovel() == codigoImovel) {
                return tipoImovel.getValorFinal();
            }
        }

        return 0;
    }

    public boolean alocacao(int codigoImovel) {
        int index = 0;

        for (Tipo_Imovel tipoImovel : Util.listaTipoImovel) {
            if (tipoImovel.getCodigoImovel() == codigoImovel) {

                if (tipoImovel.getStatus() != 'D') {
                    return false;
                }

                double valorFinal = 0;

                switch (tipoImovel.getTipo()) {
                    case 1:
                        valorFinal = tipoImovel.getAluguelBase() * 1.05;
                        break;
                    case 2:
                        valorFinal = tipoImovel.getAluguelBase() * 1.15;
                        break;
                    default:
                        valorFinal = tipoImovel.getAluguelBase() * 1.1;
                        break;
                }

                Util.listaTipoImovel.get(index).setStatus(Constantes.STATUS_ALUGADO);
                Util.listaTipoImovel.get(index).setValorFinal(valorFinal);
                break;
            }
            index++;
        }

        return true;
    }

    public boolean devolucao(int codigoImovel) {
        int index = 0;

        for (Tipo_Imovel tipoImovel : Util.listaTipoImovel) {
            if (tipoImovel.getCodigoImovel() == codigoImovel) {

                if (tipoImovel.getStatus() != 'A') {
                    return false;
                }

                Util.listaTipoImovel.get(index).setStatus(Constantes.STATUS_DISPONIVEL);
                break;
            }
            index++;
        }

        return true;
    }
}
