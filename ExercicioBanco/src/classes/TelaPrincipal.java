package classes;

import utils.Util;

public class TelaPrincipal extends javax.swing.JFrame {
    
    public TelaPrincipal() {
        initComponents();
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnSair = new javax.swing.JButton();
        lbTitulo = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        menuCadConta = new javax.swing.JMenuItem();
        menuAlterarConta = new javax.swing.JMenuItem();
        menuExcluirConta = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        menuDeposito = new javax.swing.JMenuItem();
        menuSaque = new javax.swing.JMenuItem();
        jMenu4 = new javax.swing.JMenu();
        menuConsultaConta = new javax.swing.JMenuItem();
        menuAjuda = new javax.swing.JMenu();
        menuSobre = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        menuSair = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Exercício - Banco");
        setIconImage(Util.setProjectIconImage(this.getClass())
        );
        setResizable(false);

        btnSair.setText("SAIR");
        btnSair.setFocusPainted(false);
        btnSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSairActionPerformed(evt);
            }
        });

        lbTitulo.setFont(new java.awt.Font("Cantarell", 3, 24)); // NOI18N
        lbTitulo.setText("Banco");

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/moeda.png"))); // NOI18N
        jLabel2.setPreferredSize(new java.awt.Dimension(128, 128));

        jMenu1.setText("Contas");

        menuCadConta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/novo.png"))); // NOI18N
        menuCadConta.setText("Cadastrar conta");
        menuCadConta.setPreferredSize(new java.awt.Dimension(181, 41));
        menuCadConta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCadContaActionPerformed(evt);
            }
        });
        jMenu1.add(menuCadConta);

        menuAlterarConta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/alterar.png"))); // NOI18N
        menuAlterarConta.setText("Alterar dados da conta");
        menuAlterarConta.setPreferredSize(new java.awt.Dimension(227, 41));
        menuAlterarConta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuAlterarContaActionPerformed(evt);
            }
        });
        jMenu1.add(menuAlterarConta);

        menuExcluirConta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/lixo.png"))); // NOI18N
        menuExcluirConta.setText("Excluir conta");
        menuExcluirConta.setPreferredSize(new java.awt.Dimension(145, 41));
        menuExcluirConta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuExcluirContaActionPerformed(evt);
            }
        });
        jMenu1.add(menuExcluirConta);
        jMenu1.add(jSeparator2);

        menuDeposito.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/deposito.png"))); // NOI18N
        menuDeposito.setText("Fazer depósito");
        menuDeposito.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDepositoActionPerformed(evt);
            }
        });
        jMenu1.add(menuDeposito);

        menuSaque.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/dinheiro1.png"))); // NOI18N
        menuSaque.setText("Fazer saque");
        menuSaque.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSaqueActionPerformed(evt);
            }
        });
        jMenu1.add(menuSaque);

        jMenuBar1.add(jMenu1);

        jMenu4.setText("Consultas");

        menuConsultaConta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/monitorar.png"))); // NOI18N
        menuConsultaConta.setText("Consultar contas por cliente");
        menuConsultaConta.setPreferredSize(new java.awt.Dimension(227, 41));
        menuConsultaConta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuConsultaContaActionPerformed(evt);
            }
        });
        jMenu4.add(menuConsultaConta);

        jMenuBar1.add(jMenu4);

        menuAjuda.setText("Ajuda");

        menuSobre.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/versao.png"))); // NOI18N
        menuSobre.setText("Sobre");
        menuSobre.setPreferredSize(new java.awt.Dimension(90, 41));
        menuSobre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSobreActionPerformed(evt);
            }
        });
        menuAjuda.add(menuSobre);
        menuAjuda.add(jSeparator1);

        menuSair.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/fechar.png"))); // NOI18N
        menuSair.setText("Sair");
        menuSair.setPreferredSize(new java.awt.Dimension(90, 41));
        menuSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSairActionPerformed(evt);
            }
        });
        menuAjuda.add(menuSair);

        jMenuBar1.add(menuAjuda);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(144, 144, 144)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnSair, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(lbTitulo)
                            .addGap(32, 32, 32))))
                .addContainerGap(140, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbTitulo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 55, Short.MAX_VALUE)
                .addComponent(btnSair, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(42, 42, 42))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSairActionPerformed
        System.exit(0);
    }//GEN-LAST:event_btnSairActionPerformed

    private void menuCadContaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCadContaActionPerformed
        TelaCadastroConta telaCadConta = new TelaCadastroConta();
        telaCadConta.setVisible(true);
    }//GEN-LAST:event_menuCadContaActionPerformed

    private void menuSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSairActionPerformed
        System.exit(0);
    }//GEN-LAST:event_menuSairActionPerformed

    private void menuSobreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSobreActionPerformed
        Util.abreDialogInformacao("Sobre", "Exercício de POO - Gabriel Quilice");
    }//GEN-LAST:event_menuSobreActionPerformed

    private void menuConsultaContaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuConsultaContaActionPerformed
        TelaConsultaConta telaConsultaConta = new TelaConsultaConta();
        telaConsultaConta.setVisible(true);
    }//GEN-LAST:event_menuConsultaContaActionPerformed

    private void menuAlterarContaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuAlterarContaActionPerformed
        TelaGerenciarConta telaGerenciarConta = new TelaGerenciarConta(Util.ALTERAR_CONTA);
        telaGerenciarConta.setVisible(true);
    }//GEN-LAST:event_menuAlterarContaActionPerformed

    private void menuExcluirContaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuExcluirContaActionPerformed
        TelaGerenciarConta telaGerenciarConta = new TelaGerenciarConta(Util.EXCLUIR_CONTA);
        telaGerenciarConta.setVisible(true);        
    }//GEN-LAST:event_menuExcluirContaActionPerformed

    private void menuDepositoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDepositoActionPerformed
        TelaMovimentacoes telaMovimentacoes = new TelaMovimentacoes(Util.DEPOSITO);
        telaMovimentacoes.setVisible(true);
    }//GEN-LAST:event_menuDepositoActionPerformed

    private void menuSaqueActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSaqueActionPerformed
        TelaMovimentacoes telaMovimentacoes = new TelaMovimentacoes(Util.SAQUE);
        telaMovimentacoes.setVisible(true);
    }//GEN-LAST:event_menuSaqueActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TelaPrincipal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnSair;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JLabel lbTitulo;
    private javax.swing.JMenu menuAjuda;
    private javax.swing.JMenuItem menuAlterarConta;
    private javax.swing.JMenuItem menuCadConta;
    private javax.swing.JMenuItem menuConsultaConta;
    private javax.swing.JMenuItem menuDeposito;
    private javax.swing.JMenuItem menuExcluirConta;
    private javax.swing.JMenuItem menuSair;
    private javax.swing.JMenuItem menuSaque;
    private javax.swing.JMenuItem menuSobre;
    // End of variables declaration//GEN-END:variables
}
