package utils;

import classes.Professor;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class Util {
    public static final int CONSULTA_NOME = 1;
    public static final int CONSULTA_NUM_REG = 2;
    public static final int ALTERAR_PROFESSOR = 3;
    public static final int EXCLUIR_PROFESSOR = 4;
    
    public static ArrayList<Professor> listaProfessores = new ArrayList<>();
    
    public static void abreDialogInformacao(String titulo, String mensagem) {
        JOptionPane.showMessageDialog(null, mensagem, titulo, JOptionPane.INFORMATION_MESSAGE);
    }
    
    public static void abreDialogErro(String titulo, String mensagem) {
        JOptionPane.showMessageDialog(null, mensagem, titulo, JOptionPane.ERROR_MESSAGE);
    }
  
    public static void abreDialogAtencao(String titulo, String mensagem) {
        JOptionPane.showMessageDialog(null, mensagem, titulo, JOptionPane.WARNING_MESSAGE);
    }
}
