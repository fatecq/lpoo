package utils;

public class Constantes {
    //Caminho da imagem a ser utilizada como ícone do frame
    protected static final String CAMINHO_IMAGEM = "/imagens/casa.png";
    
    //Operações
    public static final int OPERACAO_ALUGAR = 1;
    public static final int OPERACAO_DEVOLVER = 2;
    
    //Tipos
    public static final int TIPO_RESIDENCIAL = 1;
    public static final int TIPO_COMERCIAL = 2;
    public static final int TIPO_GALPAO = 3;
    
    //Status
    public static final char STATUS_DISPONIVEL = 'D';
    public static final char STATUS_ALUGADO = 'A';
}
