package controller;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import model.Relatorio;

public class RelatorioDao {

    private final ConexaoDao conexaoDao;

    public RelatorioDao() {
        this.conexaoDao = new ConexaoDao();
    }

    public ArrayList<Relatorio> getListaVendas(int idFuncionario) throws Exception {
        try {
            ArrayList<Relatorio> listaVendasRelatorio = new ArrayList<>();
            conexaoDao.conectar();

            PreparedStatement stmt = conexaoDao.getConnection().prepareStatement("SELECT DISTINCT idvenda, data_venda, "
                    + "preco_venda, tipo_pagamento "
                    + "FROM relatorio_venda WHERE idfuncionario = ? "
                    + "ORDER BY data_venda DESC");
            stmt.setInt(1, idFuncionario);

            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                Relatorio relatorio = new Relatorio();
                relatorio.setIdVenda(result.getInt("idvenda"));
                relatorio.setDataVenda(result.getString("data_venda"));
                relatorio.setPrecoVenda(result.getDouble("preco_venda"));
                relatorio.setTipoPagamento(result.getString("tipo_pagamento"));

                listaVendasRelatorio.add(relatorio);
            }

            return listaVendasRelatorio;

        } catch (SQLException e) {
            conexaoDao.desconectar();
            throw e;
        }
    }

    public ArrayList<Relatorio> getListaVendaItens(int idFuncionario, int idVenda) throws Exception {
        try {
            ArrayList<Relatorio> listaVendaItens = new ArrayList<>();
            conexaoDao.conectar();

            PreparedStatement stmt = conexaoDao.getConnection().prepareStatement("SELECT produto, quantidade, preco_item "
                    + "FROM relatorio_venda WHERE idvenda = ? AND idfuncionario = ?");
            stmt.setInt(1, idVenda);
            stmt.setInt(2, idFuncionario);

            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                Relatorio relatorio = new Relatorio();
                relatorio.setProduto(result.getString("produto"));
                relatorio.setQuantidade(result.getInt("quantidade"));
                relatorio.setPrecoItem(result.getDouble("preco_item"));

                listaVendaItens.add(relatorio);
            }

            return listaVendaItens;

        } catch (SQLException e) {
            conexaoDao.desconectar();
            throw e;
        }
    }

}
