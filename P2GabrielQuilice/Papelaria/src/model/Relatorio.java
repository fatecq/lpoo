package model;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Relatorio {
    private int idVenda, quantidade, idFuncionario;
    private double precoVenda, precoItem;
    private String produto, dataVenda, tipoPagamento, vendidoPor;
}
