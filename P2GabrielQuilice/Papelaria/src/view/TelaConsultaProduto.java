package view;

import controller.ProdutoDao;
import java.text.DecimalFormat;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import utils.Util;

public class TelaConsultaProduto extends javax.swing.JFrame {

    private TelaAlterarProduto telaAlterarProduto;

    private static DefaultTableModel modelo = new DefaultTableModel();
    private static DecimalFormat decimalFormat = new DecimalFormat("0.00");

    public TelaConsultaProduto() {
        initComponents();

        modelo = (DefaultTableModel) tableConsultaProduto.getModel();
        telaAlterarProduto = new TelaAlterarProduto();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tableConsultaProduto = new javax.swing.JTable();
        btnBuscar = new javax.swing.JButton();
        btnLimpar = new javax.swing.JButton();
        tfBuscaProduto = new javax.swing.JTextField();
        lbConsulta = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Consultar produtos");
        setIconImage(Util.setProjectIconImage(this.getClass()));
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        tableConsultaProduto.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null}
            },
            new String [] {
                "Nº identificador", "Descrição", "Código de barras", "Estoque", "Preço Unitário"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableConsultaProduto.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableConsultaProdutoMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tableConsultaProduto);

        btnBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/buscar.png"))); // NOI18N
        btnBuscar.setText("Buscar");
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });

        btnLimpar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/limpar.png"))); // NOI18N
        btnLimpar.setText("Limpar pesquisa");
        btnLimpar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimparActionPerformed(evt);
            }
        });

        lbConsulta.setText("Pesquisar:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lbConsulta)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tfBuscaProduto, javax.swing.GroupLayout.PREFERRED_SIZE, 298, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnLimpar, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnLimpar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(tfBuscaProduto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lbConsulta)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 27, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 283, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed
        try {
            if (!tfBuscaProduto.getText().isEmpty()) {
                String textoPesquisa = tfBuscaProduto.getText();

                if (!loadProdutosbyFiltro(textoPesquisa)) {
                    Util.abreDialogAtencao("Produtos", "Nenhum resultado correspondente para a pesquisa");
                    tfBuscaProduto.requestFocus();
                    tfBuscaProduto.selectAll();
                }

            } else {
                loadProdutos();
            }

            tfBuscaProduto.requestFocus();
        } catch (Exception e) {
            Util.abreDialogErro("Erro", e.getMessage());
        }
    }//GEN-LAST:event_btnBuscarActionPerformed

    private void btnLimparActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimparActionPerformed
        try {
            if (!tfBuscaProduto.getText().isEmpty()) {
                tfBuscaProduto.setText(null);
            }
            loadProdutos();
            tfBuscaProduto.requestFocus();
        } catch (Exception e) {
            Util.abreDialogErro("Erro", e.getMessage());
        }
    }//GEN-LAST:event_btnLimparActionPerformed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        try {
            loadProdutos();
        } catch (Exception e) {
            Util.abreDialogErro("Erro", e.getMessage());
        }
    }//GEN-LAST:event_formWindowOpened

    private void tableConsultaProdutoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableConsultaProdutoMouseClicked
        int linhaSelecionada = tableConsultaProduto.getSelectedRow();

        int escolha = JOptionPane.showConfirmDialog(null, "Deseja alterar os dados deste produto?", "Alterar",
                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

        if (escolha == JOptionPane.YES_OPTION) {
            abreTelaAlterarProduto((int) modelo.getValueAt(linhaSelecionada, 0));
        }
    }//GEN-LAST:event_tableConsultaProdutoMouseClicked

    public void abreTelaAlterarProduto(int codigoProduto) {
        if (!telaAlterarProduto.isVisible()) {
            telaAlterarProduto = new TelaAlterarProduto(codigoProduto);
            telaAlterarProduto.setVisible(true);
        }
    }

    public static void loadProdutos() throws Exception {
        try {
            ProdutoDao produtoDao = new ProdutoDao();

            modelo.setRowCount(0);
            produtoDao.getListaProdutos().forEach(produto -> {
                modelo.addRow(new Object[]{
                    produto.getIdProduto(),
                    produto.getDescricao(),
                    produto.getCodigoBarras(),
                    produto.getQtEstoque(),
                    "R$" + decimalFormat.format(produto.getPreco())
                });
            });

        } catch (Exception e) {
            throw e;
        }
    }

    private boolean loadProdutosbyFiltro(String filtro) throws Exception {
        try {
            ProdutoDao produtoDao = new ProdutoDao();

            modelo.setRowCount(0);
            if (Util.isInteger(filtro)) {
                produtoDao.getListaProdutos(filtro, Integer.parseInt(filtro)).forEach(produto -> {
                    modelo.addRow(new Object[]{
                        produto.getIdProduto(),
                        produto.getDescricao(),
                        produto.getCodigoBarras(),
                        produto.getQtEstoque(),
                        produto.getPreco()
                    });
                });
            } else {
                produtoDao.getListaProdutos(filtro).forEach(produto -> {
                    modelo.addRow(new Object[]{
                        produto.getIdProduto(),
                        produto.getDescricao(),
                        produto.getCodigoBarras(),
                        produto.getQtEstoque(),
                        produto.getPreco()
                    });
                });
            }

            return modelo.getRowCount() != 0;
        } catch (Exception e) {
            throw e;
        }
    }

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TelaConsultaProduto.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TelaConsultaProduto.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TelaConsultaProduto.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaConsultaProduto.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TelaConsultaProduto().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnLimpar;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lbConsulta;
    private javax.swing.JTable tableConsultaProduto;
    private javax.swing.JTextField tfBuscaProduto;
    // End of variables declaration//GEN-END:variables
}
