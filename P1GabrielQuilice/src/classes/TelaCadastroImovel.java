package classes;

import utils.Constantes;
import utils.Metodos;
import utils.Util;

public class TelaCadastroImovel extends javax.swing.JFrame {

    public TelaCadastroImovel() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lbBairro = new javax.swing.JLabel();
        tfBairro = new javax.swing.JTextField();
        lbCodigoImovel = new javax.swing.JLabel();
        tfCodigoImovel = new javax.swing.JTextField();
        btnFechar = new javax.swing.JButton();
        btnSalvar = new javax.swing.JButton();
        lbCidade = new javax.swing.JLabel();
        tfAluguelBase = new javax.swing.JTextField();
        comboBoxTipo = new javax.swing.JComboBox<>();
        lbTipo = new javax.swing.JLabel();
        lbDescricao = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        taDescricao = new javax.swing.JTextArea();
        comboBoxStatus = new javax.swing.JComboBox<>();
        lbStatus = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Cadastro de imóveis");
        setIconImage(Util.setProjectIconImage(this.getClass())
        );
        setResizable(false);

        lbBairro.setText("Bairro:");
        lbBairro.setToolTipText("");

        lbCodigoImovel.setText("Nº do imóvel:");

        btnFechar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/sair.png"))); // NOI18N
        btnFechar.setText("FECHAR");
        btnFechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFecharActionPerformed(evt);
            }
        });

        btnSalvar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/salvar.png"))); // NOI18N
        btnSalvar.setText("SALVAR");
        btnSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarActionPerformed(evt);
            }
        });

        lbCidade.setText("Aluguel base:");

        comboBoxTipo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Escolha...", "Residencial", "Comercial", "Galpão" }));

        lbTipo.setText("Tipo:");

        lbDescricao.setText("Descrição:");

        taDescricao.setColumns(20);
        taDescricao.setRows(5);
        jScrollPane1.setViewportView(taDescricao);

        comboBoxStatus.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Escolha...", "Alugado", "Disponível" }));

        lbStatus.setText("Status:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(34, 34, 34)
                        .addComponent(btnFechar, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lbCidade)
                            .addComponent(lbCodigoImovel)
                            .addComponent(lbBairro)
                            .addComponent(lbTipo)
                            .addComponent(lbDescricao)
                            .addComponent(lbStatus))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(tfBairro, javax.swing.GroupLayout.DEFAULT_SIZE, 330, Short.MAX_VALUE)
                            .addComponent(jScrollPane1)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(tfCodigoImovel, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(tfAluguelBase, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(comboBoxTipo, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(comboBoxStatus, javax.swing.GroupLayout.Alignment.LEADING, 0, 187, Short.MAX_VALUE)))))
                .addContainerGap(45, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(55, 55, 55))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbCodigoImovel)
                    .addComponent(tfCodigoImovel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(comboBoxTipo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbTipo))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbCidade)
                    .addComponent(tfAluguelBase, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(lbDescricao))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(9, 9, 9)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfBairro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbBairro))
                .addGap(14, 14, 14)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(comboBoxStatus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbStatus))
                .addGap(27, 27, 27)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnFechar, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents


    private void btnFecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFecharActionPerformed
        dispose();
    }//GEN-LAST:event_btnFecharActionPerformed

    private void btnSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarActionPerformed
        try {
            int codigoImovel = Integer.parseInt(tfCodigoImovel.getText());

            if (Util.listaTipoImovel.stream().anyMatch(tipoImovel -> tipoImovel.getCodigoImovel() == codigoImovel)) {
                Util.abreDialogAtencao("Atenção", "Já existe um imóvel com o nº  " + codigoImovel);
                tfCodigoImovel.requestFocus();
                return;
            }

            if (comboBoxTipo.getSelectedIndex() == 0) {
                Util.abreDialogAtencao("Atenção", "Selecione o tipo de imóvel!");
                return;
            }

            if (comboBoxStatus.getSelectedIndex() == 0) {
                Util.abreDialogAtencao("Atenção", "Selecione o status do imóvel!");
                return;
            }

            Tipo_Imovel tipoImovel = new Tipo_Imovel();
            tipoImovel.setCodigoImovel(codigoImovel);
            tipoImovel.setTipo(comboBoxTipo.getSelectedIndex());
            tipoImovel.setAluguelBase(Double.parseDouble(tfAluguelBase.getText()));
            tipoImovel.setDescricao(taDescricao.getText());
            tipoImovel.setBairro(tfBairro.getText());

            switch (comboBoxStatus.getSelectedIndex()) {
                case 1:
                    double valorFinal = 0;

                    switch (comboBoxTipo.getSelectedIndex()) {
                        case 1:
                            valorFinal = Double.parseDouble(tfAluguelBase.getText()) * 1.05;
                            break;
                        case 2:
                            valorFinal = Double.parseDouble(tfAluguelBase.getText()) * 1.15;
                            break;
                        default:
                            valorFinal = Double.parseDouble(tfAluguelBase.getText()) * 1.1;
                            break;
                    }
                    tipoImovel.setStatus(Constantes.STATUS_ALUGADO);
                    tipoImovel.setValorFinal(valorFinal);
                    break;

                case 2:
                    tipoImovel.setStatus(Constantes.STATUS_DISPONIVEL);
                    tipoImovel.setValorFinal(0D);
                    break;
            }

            Metodos metodos = new Metodos(tipoImovel);
            metodos.salvar();
            limpaCampos();
        } catch (Exception e) {
            Util.abreDialogErro("Erro", e.getMessage());
        }
    }//GEN-LAST:event_btnSalvarActionPerformed

    private void limpaCampos() {
        tfCodigoImovel.setText(null);
        tfBairro.setText(null);
        taDescricao.setText(null);
        tfBairro.setText(null);
        tfAluguelBase.setText(null);
        comboBoxTipo.setSelectedIndex(0);
        comboBoxStatus.setSelectedIndex(0);

        tfCodigoImovel.requestFocus();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TelaCadastroImovel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TelaCadastroImovel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TelaCadastroImovel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaCadastroImovel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TelaCadastroImovel().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnFechar;
    private javax.swing.JButton btnSalvar;
    private javax.swing.JComboBox<String> comboBoxStatus;
    private javax.swing.JComboBox<String> comboBoxTipo;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lbBairro;
    private javax.swing.JLabel lbCidade;
    private javax.swing.JLabel lbCodigoImovel;
    private javax.swing.JLabel lbDescricao;
    private javax.swing.JLabel lbStatus;
    private javax.swing.JLabel lbTipo;
    private javax.swing.JTextArea taDescricao;
    private javax.swing.JTextField tfAluguelBase;
    private javax.swing.JTextField tfBairro;
    private javax.swing.JTextField tfCodigoImovel;
    // End of variables declaration//GEN-END:variables
}
