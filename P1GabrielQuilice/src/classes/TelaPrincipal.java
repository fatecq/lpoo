package classes;

import utils.Constantes;
import utils.Metodos;
import utils.Util;

public class TelaPrincipal extends javax.swing.JFrame {

    private Metodos metodos;
    private TelaOperacoes telaOperacoes;
    private TelaCadastroImovel telaCadastroImovel;
    private TelaConsultaImovel telaConsultaImovel;
    private TelaGerarRelatorio telaGerarRelatorio;

    public TelaPrincipal() {
        initComponents();

        metodos = new Metodos();
        telaOperacoes = new TelaOperacoes();
        telaCadastroImovel = new TelaCadastroImovel();
        telaConsultaImovel = new TelaConsultaImovel();
        telaGerarRelatorio = new TelaGerarRelatorio();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnSair = new javax.swing.JButton();
        lbTitulo = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        menuCadImovel = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        menuAlocacao = new javax.swing.JMenuItem();
        menuDevolucao = new javax.swing.JMenuItem();
        jMenu4 = new javax.swing.JMenu();
        menuConsultaImovel = new javax.swing.JMenuItem();
        menuRelatorio = new javax.swing.JMenuItem();
        menuAjuda = new javax.swing.JMenu();
        menuSobre = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        menuSair = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Prova - P1");
        setIconImage(Util.setProjectIconImage(this.getClass())
        );
        setResizable(false);

        btnSair.setText("SAIR");
        btnSair.setFocusPainted(false);
        btnSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSairActionPerformed(evt);
            }
        });

        lbTitulo.setFont(new java.awt.Font("Cantarell", 3, 24)); // NOI18N
        lbTitulo.setText("Prova P1");

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/casa.png"))); // NOI18N

        jMenu1.setText("Imóveis");

        menuCadImovel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/novo.png"))); // NOI18N
        menuCadImovel.setText("Cadastrar imóvel");
        menuCadImovel.setPreferredSize(new java.awt.Dimension(181, 41));
        menuCadImovel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCadImovelActionPerformed(evt);
            }
        });
        jMenu1.add(menuCadImovel);
        jMenu1.add(jSeparator2);

        menuAlocacao.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/produtos.png"))); // NOI18N
        menuAlocacao.setText("Alugar imóvel");
        menuAlocacao.setPreferredSize(new java.awt.Dimension(181, 41));
        menuAlocacao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuAlocacaoActionPerformed(evt);
            }
        });
        jMenu1.add(menuAlocacao);

        menuDevolucao.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/log_out.gif"))); // NOI18N
        menuDevolucao.setText("Devolver imóvel");
        menuDevolucao.setPreferredSize(new java.awt.Dimension(181, 41));
        menuDevolucao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDevolucaoActionPerformed(evt);
            }
        });
        jMenu1.add(menuDevolucao);

        jMenuBar1.add(jMenu1);

        jMenu4.setText("Consultas");

        menuConsultaImovel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/monitorar.png"))); // NOI18N
        menuConsultaImovel.setText("Consultar imóveis");
        menuConsultaImovel.setPreferredSize(new java.awt.Dimension(181, 41));
        menuConsultaImovel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuConsultaImovelActionPerformed(evt);
            }
        });
        jMenu4.add(menuConsultaImovel);

        menuRelatorio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/listar.png"))); // NOI18N
        menuRelatorio.setText("Gerar relatório");
        menuRelatorio.setPreferredSize(new java.awt.Dimension(181, 41));
        menuRelatorio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuRelatorioActionPerformed(evt);
            }
        });
        jMenu4.add(menuRelatorio);

        jMenuBar1.add(jMenu4);

        menuAjuda.setText("Ajuda");

        menuSobre.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/versao.png"))); // NOI18N
        menuSobre.setText("Sobre");
        menuSobre.setPreferredSize(new java.awt.Dimension(90, 41));
        menuSobre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSobreActionPerformed(evt);
            }
        });
        menuAjuda.add(menuSobre);
        menuAjuda.add(jSeparator1);

        menuSair.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/fechar.png"))); // NOI18N
        menuSair.setText("Sair");
        menuSair.setPreferredSize(new java.awt.Dimension(90, 41));
        menuSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSairActionPerformed(evt);
            }
        });
        menuAjuda.add(menuSair);

        jMenuBar1.add(menuAjuda);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(144, 144, 144)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnSair, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addContainerGap(140, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lbTitulo)
                .addGap(155, 155, 155))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbTitulo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 55, Short.MAX_VALUE)
                .addComponent(btnSair, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(42, 42, 42))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSairActionPerformed
        metodos.encerrarPrograma();
    }//GEN-LAST:event_btnSairActionPerformed

    private void menuCadImovelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCadImovelActionPerformed
        if (!telaCadastroImovel.isVisible()) {
            telaCadastroImovel = new TelaCadastroImovel();
            telaCadastroImovel.setVisible(true);
        }
    }//GEN-LAST:event_menuCadImovelActionPerformed

    private void menuSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSairActionPerformed
        metodos.encerrarPrograma();
    }//GEN-LAST:event_menuSairActionPerformed

    private void menuSobreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSobreActionPerformed
        Util.abreDialogInformacao("Sobre", "Prova de POO - Gabriel Quilice");
    }//GEN-LAST:event_menuSobreActionPerformed

    private void menuConsultaImovelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuConsultaImovelActionPerformed
        if (!telaConsultaImovel.isVisible()) {
            telaConsultaImovel = new TelaConsultaImovel();
            telaConsultaImovel.setVisible(true);
        }
    }//GEN-LAST:event_menuConsultaImovelActionPerformed

    private void menuAlocacaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuAlocacaoActionPerformed
        if (!telaOperacoes.isVisible()) {
            telaOperacoes = new TelaOperacoes(Constantes.OPERACAO_ALUGAR);
            telaOperacoes.setVisible(true);
        }
    }//GEN-LAST:event_menuAlocacaoActionPerformed

    private void menuDevolucaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDevolucaoActionPerformed
        if (!telaOperacoes.isVisible()) {
            telaOperacoes = new TelaOperacoes(Constantes.OPERACAO_DEVOLVER);
            telaOperacoes.setVisible(true);
        }
    }//GEN-LAST:event_menuDevolucaoActionPerformed

    private void menuRelatorioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuRelatorioActionPerformed
        if (!telaGerarRelatorio.isVisible()){
            telaGerarRelatorio = new TelaGerarRelatorio();
            telaGerarRelatorio.setVisible(true);
        }
    }//GEN-LAST:event_menuRelatorioActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TelaPrincipal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnSair;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JLabel lbTitulo;
    private javax.swing.JMenu menuAjuda;
    private javax.swing.JMenuItem menuAlocacao;
    private javax.swing.JMenuItem menuCadImovel;
    private javax.swing.JMenuItem menuConsultaImovel;
    private javax.swing.JMenuItem menuDevolucao;
    private javax.swing.JMenuItem menuRelatorio;
    private javax.swing.JMenuItem menuSair;
    private javax.swing.JMenuItem menuSobre;
    // End of variables declaration//GEN-END:variables
}
