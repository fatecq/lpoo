package controller;

import java.sql.PreparedStatement;
import utils.Util;

public class LoginDao {
    private final ConexaoDao conexaoDao;
    
    public LoginDao() {
        this. conexaoDao = new ConexaoDao();
    }
    
    public boolean verifyLogin(String usuario, String senha) throws Exception {
        try {
            conexaoDao.conectar();
            PreparedStatement stmt = conexaoDao.getConnection().prepareStatement("SELECT idfuncionario, nome FROM funcionario"
                    + " WHERE sys_user=? AND sys_password=? AND fg_ativo = 'S'");
            stmt.setString(1, usuario.toLowerCase());
            stmt.setString(2, senha);
            
            boolean isCorrect = stmt.executeQuery().next();
            
            if(isCorrect) {
                Util.idFuncionario = stmt.getResultSet().getInt("idfuncionario");
                Util.nomeFuncionario = stmt.getResultSet().getString("nome");
            }
            
            conexaoDao.desconectar();
            
            return isCorrect;
            
        } catch (Exception e) {
            Util.abreDialogErro("Erro", e.getMessage());
            conexaoDao.desconectar();
            throw e;
        }
    }
    
}
