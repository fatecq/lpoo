package controller;

import java.sql.*;
import lombok.Getter;
import utils.Util;

public class ConexaoDao {

    private final String ENDERECO_SERVIDOR = "localhost:3306";
    private final String BD = "papelaria";
    private final String URL = "jdbc:mysql://" + ENDERECO_SERVIDOR + "/" + BD;
    private final String USUARIO = "root";
    private final String SENHA = "mysql";

    @Getter
    private Connection connection;

    public void conectar() {
        try {
            //estou usando o mysql 8, por isso a versão do driver usada aqui é diferente da usada em aula
            Class.forName("org.gjt.mm.mysql.Driver"); 

            connection = DriverManager.getConnection(URL, USUARIO, SENHA);
            connection.setAutoCommit(false);

        } catch (ClassNotFoundException e) {
            Util.abreDialogErro("Erro", "O driver especificado não foi encontrado!" );
            System.out.println("ConexaoDao - Erro: " + e.getMessage());
        } catch (SQLException e) {
            Util.abreDialogErro("Erro", "Não foi possível se conectar ao banco de dados!");
            System.out.println("ConexaoDao - Erro: " + e.getMessage());
        }
    }

    public boolean desconectar() {
        try {
            connection.close();
            return true;
        } catch (SQLException e) {
            return false;
        }
    }

}
