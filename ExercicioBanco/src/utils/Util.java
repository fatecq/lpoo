package utils;

import classes.Conta;
import java.awt.Image;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

public class Util {
    
    //Caminho da imagem a ser utilizada como ícone do frame
    private static final String CAMINHO_IMAGEM = "/imagens/moeda.png";
    
    public static final int SAQUE = 1;
    public static final int DEPOSITO = 2;
    public static final int ALTERAR_CONTA = 3;
    public static final int EXCLUIR_CONTA = 4;
    
    public static ArrayList<Conta> listaContas = new ArrayList<>();
    
    public static void abreDialogInformacao(String titulo, String mensagem) {
        JOptionPane.showMessageDialog(null, mensagem, titulo, JOptionPane.INFORMATION_MESSAGE);
    }
    
    public static void abreDialogErro(String titulo, String mensagem) {
        JOptionPane.showMessageDialog(null, mensagem, titulo, JOptionPane.ERROR_MESSAGE);
    }
  
    public static void abreDialogAtencao(String titulo, String mensagem) {
        JOptionPane.showMessageDialog(null, mensagem, titulo, JOptionPane.WARNING_MESSAGE);
    }
    
    //Define o ícone do frame
    public static Image setProjectIconImage(Class classe) {
        return new ImageIcon(classe.getResource(CAMINHO_IMAGEM)).getImage();
    }
}
