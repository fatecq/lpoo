package classes;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Imovel {

    private int codigoImovel, tipo;
    private double aluguelBase;

}
