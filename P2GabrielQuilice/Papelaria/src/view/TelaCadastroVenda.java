package view;

import controller.ProdutoDao;
import controller.TipoPagamentoDao;
import controller.VendaDao;
import java.text.DecimalFormat;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import model.Produto;
import model.ProdutoVenda;
import model.TipoPagamento;
import utils.Util;

public class TelaCadastroVenda extends javax.swing.JFrame {

    private static ArrayList<ProdutoVenda> listaProdutoVenda = new ArrayList<>();
    private static ArrayList<TipoPagamento> listaTipoPagamento = new ArrayList<>();
    private static DefaultTableModel modelo = new DefaultTableModel();
    private static final DecimalFormat decimalFormat = new DecimalFormat("0.00");
    private Produto produto = new Produto();

    public TelaCadastroVenda() {
        initComponents();

        modelo = (DefaultTableModel) tableProdutosVenda.getModel();
        modelo.setRowCount(0);

        tfCodigo.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent de) {
                if (!btnAcaoProduto.getText().equals("PROCURAR")) {
                    btnAcaoProduto.setText("PROCURAR");
                    lbDescricaoProduto.setText(null);
                    tfQuantidade.setText(null);
                }
            }

            @Override
            public void removeUpdate(DocumentEvent de) {
                if (!btnAcaoProduto.getText().equals("PROCURAR")) {
                    btnAcaoProduto.setText("PROCURAR");
                    lbDescricaoProduto.setText(null);
                    tfQuantidade.setText(null);
                }
            }

            @Override
            public void changedUpdate(DocumentEvent de) {

            }
        });

        loadTipoPagamento();
        comboBoxPagamento.setSelectedIndex(0);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnFechar = new javax.swing.JButton();
        btnFinalizar = new javax.swing.JButton();
        comboBoxPagamento = new javax.swing.JComboBox<>();
        lbTipoPagamento = new javax.swing.JLabel();
        pnProduto = new javax.swing.JPanel();
        btnAcaoProduto = new javax.swing.JButton();
        btnLimpar = new javax.swing.JButton();
        lbCodigo = new javax.swing.JLabel();
        tfCodigo = new javax.swing.JTextField();
        lbDescricaoProduto = new javax.swing.JLabel();
        lbQuantidade = new javax.swing.JLabel();
        tfQuantidade = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableProdutosVenda = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Cadastro de vendas");
        setIconImage(Util.setProjectIconImage(this.getClass())
        );
        setResizable(false);

        btnFechar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/sair.png"))); // NOI18N
        btnFechar.setText("FECHAR");
        btnFechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFecharActionPerformed(evt);
            }
        });

        btnFinalizar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/salvar.png"))); // NOI18N
        btnFinalizar.setText("FINALIZAR");
        btnFinalizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFinalizarActionPerformed(evt);
            }
        });

        lbTipoPagamento.setText("Tipo de pagamento:");

        pnProduto.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        btnAcaoProduto.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnAcaoProduto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/confirmar.png"))); // NOI18N
        btnAcaoProduto.setText("PROCURAR");
        btnAcaoProduto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAcaoProdutoActionPerformed(evt);
            }
        });

        btnLimpar.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnLimpar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/limpar.png"))); // NOI18N
        btnLimpar.setText("LIMPAR");
        btnLimpar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimparActionPerformed(evt);
            }
        });

        lbCodigo.setText("Código:");
        lbCodigo.setToolTipText("");

        lbDescricaoProduto.setFont(new java.awt.Font("Cantarell", 3, 14)); // NOI18N
        lbDescricaoProduto.setForeground(new java.awt.Color(16, 126, 7));

        lbQuantidade.setText("Quantidade:");

        javax.swing.GroupLayout pnProdutoLayout = new javax.swing.GroupLayout(pnProduto);
        pnProduto.setLayout(pnProdutoLayout);
        pnProdutoLayout.setHorizontalGroup(
            pnProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnProdutoLayout.createSequentialGroup()
                .addGroup(pnProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnProdutoLayout.createSequentialGroup()
                        .addGap(33, 33, 33)
                        .addGroup(pnProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pnProdutoLayout.createSequentialGroup()
                                .addGap(71, 71, 71)
                                .addComponent(btnAcaoProduto, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(27, 27, 27)
                                .addComponent(btnLimpar, javax.swing.GroupLayout.PREFERRED_SIZE, 152, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(pnProdutoLayout.createSequentialGroup()
                                .addComponent(lbCodigo)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(pnProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lbDescricaoProduto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addGroup(pnProdutoLayout.createSequentialGroup()
                                        .addGroup(pnProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(tfQuantidade, javax.swing.GroupLayout.PREFERRED_SIZE, 402, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(tfCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 403, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(0, 0, Short.MAX_VALUE))))))
                    .addGroup(pnProdutoLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(lbQuantidade)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        pnProdutoLayout.setVerticalGroup(
            pnProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnProdutoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pnProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbCodigo)
                    .addComponent(tfCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbDescricaoProduto)
                .addGap(18, 18, Short.MAX_VALUE)
                .addGroup(pnProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbQuantidade)
                    .addComponent(tfQuantidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(27, 27, 27)
                .addGroup(pnProdutoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAcaoProduto, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnLimpar, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        tableProdutosVenda.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Produto", "Quantidade", "Valor Unitário"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tableProdutosVenda);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(30, 46, Short.MAX_VALUE)
                        .addComponent(lbTipoPagamento)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(comboBoxPagamento, javax.swing.GroupLayout.PREFERRED_SIZE, 330, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(91, 91, 91)
                        .addComponent(btnFechar, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(62, 62, 62)
                        .addComponent(btnFinalizar, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addGap(55, 55, 55))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnProduto, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnProduto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbTipoPagamento)
                    .addComponent(comboBoxPagamento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnFechar, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnFinalizar, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(21, 21, 21))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents


    private void btnFecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFecharActionPerformed
        dispose();
    }//GEN-LAST:event_btnFecharActionPerformed

    private void btnFinalizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFinalizarActionPerformed
        try {
            if (listaProdutoVenda.isEmpty()) {
                Util.abreDialogAtencao("Venda", "Adicione ao menos um produto para\nfinalizar a venda!");
                return;
            }

            VendaDao vendaDao = new VendaDao();
            final int[] idTipoPagamento = {0};

            listaTipoPagamento.stream().forEach(item -> {
                if (comboBoxPagamento.getSelectedItem().toString().equals(item.getDescricao())) {
                    idTipoPagamento[0] = item.getIdTipo();
                    return;
                }
            });

            Util.abreDialogInformacao("Venda realizada!", "Venda realizada com sucesso!\n"
                    + "Valor total: R$" + decimalFormat.format(vendaDao.cadastrar(listaProdutoVenda, idTipoPagamento[0])));
            limpaCampos(true);
        } catch (Exception e) {
            Util.abreDialogErro("Erro", e.getMessage());
        }
    }//GEN-LAST:event_btnFinalizarActionPerformed

    private void btnAcaoProdutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAcaoProdutoActionPerformed
        try {
            String codigo = tfCodigo.getText().trim();
            ProdutoDao produtoDao = new ProdutoDao();

            if (codigo.isEmpty()) {
                return;
            }

            if (btnAcaoProduto.getText().equals("PROCURAR")) {
                produto = produtoDao.procurarProduto(codigo, true);
                if (produto == null) {
                    Util.abreDialogAtencao("Atenção", "Produto inexistente ou em falta!");
                    return;
                }
                lbDescricaoProduto.setText(produto.getDescricao());
                btnAcaoProduto.setText("ADICIONAR");
            } else {
                String qtde = tfQuantidade.getText().trim();
                if (qtde.isEmpty() | Integer.parseInt(qtde) == 0) {
                    Util.abreDialogAtencao("Atenção", "Informe uma quantidade válida!");
                    return;
                }
                if (produto.getQtEstoque() < Integer.parseInt(qtde)) {
                    Util.abreDialogAtencao("Atenção", "Quantidade indisponível para venda!");
                    return;
                }

                ProdutoVenda produtoVenda = new ProdutoVenda();
                produtoVenda.setCodigoProduto(produto.getIdProduto());
                produtoVenda.setPrecoProduto(produto.getPreco());
                produtoVenda.setQuantidade(Integer.parseInt(qtde));

                if (listaProdutoVenda.stream().anyMatch(item -> item.getCodigoProduto() == produtoVenda.getCodigoProduto())) {
                    int escolha = JOptionPane.showConfirmDialog(null, "Este produto já está previsto para esta venda. \n"
                            + "Deseja alterar por esse novo cadastro?", "Alterar",
                            JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

                    if (escolha == JOptionPane.NO_OPTION) {
                        limpaCampos(false);
                        return;
                    } else {
                        listaProdutoVenda.removeIf(item -> item.getCodigoProduto() == produtoVenda.getCodigoProduto());
                    }
                }

                listaProdutoVenda.add(produtoVenda);
                
                loadProdutosVenda();

                limpaCampos(false);
            }
        } catch (Exception e) {
            Util.abreDialogErro("Erro", e.getMessage());
        }
    }//GEN-LAST:event_btnAcaoProdutoActionPerformed

    private void btnLimparActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimparActionPerformed
        limpaCampos(false);
    }//GEN-LAST:event_btnLimparActionPerformed

    private void limpaCampos(boolean isFinalizada) {
        tfCodigo.setText(null);
        tfQuantidade.setText(null);
        comboBoxPagamento.setSelectedIndex(0);
        if (isFinalizada) {
            modelo.setRowCount(0);
            listaProdutoVenda.clear();
        }
    }

    private void loadProdutosVenda() {
        modelo.setRowCount(0);
        
        listaProdutoVenda.forEach(item -> {
            modelo.addRow(new Object[]{
                item.getCodigoProduto(),
                item.getQuantidade(),
                "R$" + decimalFormat.format(item.getPrecoProduto())
            });
        });
    }

    private void loadTipoPagamento() {
        try {
            TipoPagamentoDao pagamentoDao = new TipoPagamentoDao();

            listaTipoPagamento = pagamentoDao.getListaTipoPagamento();

            listaTipoPagamento.forEach(tipo -> {
                comboBoxPagamento.addItem(tipo.getDescricao());
            });
        } catch (Exception e) {
            Util.abreDialogErro("Erro", e.getMessage());
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TelaCadastroVenda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TelaCadastroVenda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TelaCadastroVenda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaCadastroVenda.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TelaCadastroVenda().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAcaoProduto;
    private javax.swing.JButton btnFechar;
    private javax.swing.JButton btnFinalizar;
    private javax.swing.JButton btnLimpar;
    private javax.swing.JComboBox<String> comboBoxPagamento;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lbCodigo;
    private javax.swing.JLabel lbDescricaoProduto;
    private javax.swing.JLabel lbQuantidade;
    private javax.swing.JLabel lbTipoPagamento;
    private javax.swing.JPanel pnProduto;
    private javax.swing.JTable tableProdutosVenda;
    private javax.swing.JTextField tfCodigo;
    private javax.swing.JTextField tfQuantidade;
    // End of variables declaration//GEN-END:variables

}
