package controller;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import model.ProdutoVenda;
import utils.Util;

public class VendaDao {

    private final ConexaoDao conexaoDao;

    public VendaDao() {
        this.conexaoDao = new ConexaoDao();
    }

    public Double cadastrar(ArrayList<ProdutoVenda> listaProdutos, int tipoPag) throws Exception {
        try {
            conexaoDao.conectar();

            PreparedStatement stmt = conexaoDao.getConnection().prepareStatement(
                    "SELECT MAX(idvenda)+1 AS next_id FROM venda");

            stmt.executeQuery().next();
            int idVenda = stmt.getResultSet().getInt("next_id");

            PreparedStatement stmt2 = conexaoDao.getConnection().prepareStatement(
                    "INSERT INTO venda(f_idfuncionario, tp_idpagamento, data_venda, preco_venda) VALUES "
                    + "(?, ?, NOW(), 0.00)");
            stmt2.setInt(1, Util.idFuncionario);
            stmt2.setInt(2, tipoPag);

            stmt2.execute();

            for (ProdutoVenda produtoVenda : listaProdutos) {
                PreparedStatement stmt3 = conexaoDao.getConnection().prepareStatement(
                        "INSERT INTO venda_item(v_idvenda, p_idproduto, quantidade, total_preco_item) VALUES "
                        + "(?, ?, ?, ?)");
                stmt3.setInt(1, idVenda);
                stmt3.setInt(2, produtoVenda.getCodigoProduto());
                stmt3.setInt(3, produtoVenda.getQuantidade());
                stmt3.setDouble(4, produtoVenda.getPrecoProduto() * produtoVenda.getQuantidade());

                stmt3.execute();

            }
            
            conexaoDao.getConnection().commit();

            PreparedStatement stmt4 = conexaoDao.getConnection().prepareStatement(
                    "SELECT preco_venda FROM venda WHERE idvenda = ?");
            stmt4.setInt(1, idVenda);

            ResultSet result = stmt4.executeQuery();
            result.next();
            double preco_venda = result.getDouble("preco_venda");

            conexaoDao.desconectar();

            return preco_venda;

        } catch (Exception e) {
            conexaoDao.getConnection().rollback();
            conexaoDao.desconectar();
            throw e;
        }
    }

}
