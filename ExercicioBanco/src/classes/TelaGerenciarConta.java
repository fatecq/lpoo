package classes;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import utils.Util;

public class TelaGerenciarConta extends javax.swing.JFrame {

    private static int tipoAcao;
    private static DefaultTableModel modelo = new DefaultTableModel();

    public TelaGerenciarConta(int tipoAcao) {
        this.tipoAcao = tipoAcao;
        initComponents();

        modelo = (DefaultTableModel) tableGerenciaContas.getModel();
        if (tipoAcao == Util.ALTERAR_CONTA) {
            this.setTitle("Alterar conta");
            btnAcao.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/alterar.png")));
            btnAcao.setText("ALTERAR");
        } else {
            this.setTitle("Excluir conta");
        }

        loadContas();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tableGerenciaContas = new javax.swing.JTable();
        btnAcao = new javax.swing.JButton();
        btnFechar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Gerenciar contas");
        setIconImage(Util.setProjectIconImage(this.getClass())
        );
        setResizable(false);

        tableGerenciaContas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null}
            },
            new String [] {
                "Nº da conta", "Nome", "CPF"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tableGerenciaContas);

        btnAcao.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/lixo.png"))); // NOI18N
        btnAcao.setText("EXCLUIR");
        btnAcao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAcaoActionPerformed(evt);
            }
        });

        btnFechar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/sair.png"))); // NOI18N
        btnFechar.setText("FECHAR");
        btnFechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFecharActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 463, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(21, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnFechar, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(26, 26, 26)
                        .addComponent(btnAcao, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(102, 102, 102))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAcao, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnFechar, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(21, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnAcaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAcaoActionPerformed
        try {
            int linhaSelecionada = tableGerenciaContas.getSelectedRow();

            if (linhaSelecionada == -1) {
                String titulo = (tipoAcao == Util.EXCLUIR_CONTA ? "Exclusão" : "Edição");
                Util.abreDialogAtencao(titulo, "Selecione uma linha da tabela para continuar!");
                return;
            }

            int numConta = (int) modelo.getValueAt(linhaSelecionada, 0);

            if (tipoAcao == Util.EXCLUIR_CONTA) {
                int escolha = JOptionPane.showConfirmDialog(null, "Deseja excluir a conta selecionada?", "Excluir",
                        JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

                if (escolha == JOptionPane.YES_OPTION) {
                    modelo.removeRow(linhaSelecionada);
                    Metodos_Contas.excluir(numConta);
                    loadContas();
                } else {
                    tableGerenciaContas.clearSelection();
                }

            } else {
                int escolha = JOptionPane.showConfirmDialog(null, "Deseja alterar a conta selecionada?", "Alterar",
                        JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

                if (escolha == JOptionPane.YES_OPTION) {
                    abreTelaAlterarConta(numConta);
                } else {
                    tableGerenciaContas.clearSelection();
                }
            }

        } catch (Exception e) {
            Util.abreDialogErro("Erro", e.getMessage());
        }
    }//GEN-LAST:event_btnAcaoActionPerformed

    private void btnFecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFecharActionPerformed
        dispose();
    }//GEN-LAST:event_btnFecharActionPerformed

    private void abreTelaAlterarConta(int numConta) {
        TelaAlterarConta telaAlterarConta = new TelaAlterarConta(numConta);
        telaAlterarConta.setVisible(true);
    }

    public static void loadContas() {
        try {
            modelo.setRowCount(0);

            Util.listaContas.forEach(conta -> {
                modelo.addRow(new Object[]{
                    conta.getNumConta(),
                    conta.getNomeCliente(),
                    conta.getCPF()
                });
            });
        } catch (Exception e) {
            Util.abreDialogErro("Erro", e.getMessage());
        }
    }

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TelaGerenciarConta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TelaGerenciarConta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TelaGerenciarConta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaGerenciarConta.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TelaGerenciarConta(tipoAcao).setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAcao;
    private javax.swing.JButton btnFechar;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tableGerenciaContas;
    // End of variables declaration//GEN-END:variables
}
