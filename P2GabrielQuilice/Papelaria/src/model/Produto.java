package model;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Produto {
    private int idProduto, qtEstoque;
    private String descricao, codigoBarras, fgAtivo;
    private double preco;
}
