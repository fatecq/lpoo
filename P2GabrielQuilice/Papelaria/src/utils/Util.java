package utils;

import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

public class Util {
    public static int idFuncionario = 0;
    public static String nomeFuncionario = "";    
    
    public static void encerrarPrograma() {
        int escolha = JOptionPane.showConfirmDialog(null, "Deseja sair e encerrar o programa?", "Sair",
                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

        if (escolha == JOptionPane.YES_OPTION) {
            System.exit(0);
        }
    }
    
    public static boolean isInteger(String valor) {
        try {
            Integer.parseInt(valor);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    } 
    
    public static void abreDialogInformacao(String titulo, String mensagem) {
        JOptionPane.showMessageDialog(null, mensagem, titulo, JOptionPane.INFORMATION_MESSAGE);
    }
    
    public static void abreDialogErro(String titulo, String mensagem) {
        JOptionPane.showMessageDialog(null, mensagem, titulo, JOptionPane.ERROR_MESSAGE);
    }
  
    public static void abreDialogAtencao(String titulo, String mensagem) {
        JOptionPane.showMessageDialog(null, mensagem, titulo, JOptionPane.WARNING_MESSAGE);
    }
    
    //Define o ícone do frame
    public static Image setProjectIconImage(Class classe) {
        return new ImageIcon(classe.getResource(Constantes.CAMINHO_IMAGEM)).getImage();
    }
}
