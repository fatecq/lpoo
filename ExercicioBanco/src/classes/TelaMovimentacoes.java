package classes;

import utils.Util;

public class TelaMovimentacoes extends javax.swing.JFrame {

    private static int tipoMovimento;

    public TelaMovimentacoes(int tipoMovimento) {
        this.tipoMovimento = tipoMovimento;

        initComponents();

        if (tipoMovimento == Util.SAQUE) {
            setTitle("Realizar saque");
            btnMovimento.setText("SAQUE");
        } else {
            setTitle("Realizar depósito");
            btnMovimento.setText("DEPÓSITO");
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnFechar = new javax.swing.JButton();
        btnMovimento = new javax.swing.JButton();
        lbNumConta = new javax.swing.JLabel();
        tfNumConta = new javax.swing.JTextField();
        lbValor = new javax.swing.JLabel();
        tfValor = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Realizar depósito");
        setIconImage(Util.setProjectIconImage(this.getClass())
        );
        setResizable(false);

        btnFechar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/sair.png"))); // NOI18N
        btnFechar.setText("FECHAR");
        btnFechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFecharActionPerformed(evt);
            }
        });

        btnMovimento.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/dinheiro2.png"))); // NOI18N
        btnMovimento.setText("DEPÓSITO");
        btnMovimento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMovimentoActionPerformed(evt);
            }
        });

        lbNumConta.setText("Nº da conta:");

        lbValor.setText("Valor:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnFechar, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(42, 42, 42)
                        .addComponent(btnMovimento, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lbValor)
                            .addComponent(lbNumConta))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(tfNumConta, javax.swing.GroupLayout.PREFERRED_SIZE, 198, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(tfValor, javax.swing.GroupLayout.PREFERRED_SIZE, 198, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(22, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbNumConta)
                    .addComponent(tfNumConta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbValor)
                    .addComponent(tfValor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 34, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnMovimento, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnFechar, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(27, 27, 27))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnMovimentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMovimentoActionPerformed
        try {
            int numConta = Integer.parseInt(tfNumConta.getText());
            double valor = Double.parseDouble(tfValor.getText());

            if (!validaConta(numConta)) {
                Util.abreDialogAtencao("Atenção", "Não existe nenhum cliente com a conta " + numConta + ", favor verifique!");
                limpaCampos();
                return;
            }

            if (tipoMovimento == Util.SAQUE) {

                if (!Metodos_Contas.saque(numConta, valor)) {
                    Util.abreDialogAtencao("Atenção", "Não foi possível fazer o saque, favor verifique!");
                } else {
                    Util.abreDialogInformacao("Saque", "Saque concluído com sucesso!");
                    limpaCampos();
                }

            } else {

                if (!Metodos_Contas.deposito(numConta, valor)) {
                    Util.abreDialogAtencao("Atenção", "Não foi possível fazer o depósito, favor verifique!");
                } else {
                    Util.abreDialogInformacao("Depósito", "Depósito concluído com sucesso!");
                    limpaCampos();
                }

            }
        } catch (Exception e) {
            Util.abreDialogErro("Erro", e.getMessage());
        }
    }//GEN-LAST:event_btnMovimentoActionPerformed

    private void btnFecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFecharActionPerformed
        dispose();
    }//GEN-LAST:event_btnFecharActionPerformed

    private boolean validaConta(int numConta) {
        return Util.listaContas.stream().anyMatch(conta -> conta.getNumConta() == numConta);
    }

    private void limpaCampos() {
        tfNumConta.setText(null);
        tfValor.setText(null);
        tfNumConta.requestFocus();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TelaMovimentacoes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TelaMovimentacoes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TelaMovimentacoes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaMovimentacoes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TelaMovimentacoes(tipoMovimento).setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnFechar;
    private javax.swing.JButton btnMovimento;
    private javax.swing.JLabel lbNumConta;
    private javax.swing.JLabel lbValor;
    private javax.swing.JTextField tfNumConta;
    private javax.swing.JTextField tfValor;
    // End of variables declaration//GEN-END:variables
}
