package classes;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Professor {
    
    private int numRegistro, titulacao;
    private double valorHoraAula, totalHoraAula, totalGeralSemanal;
    private String nomeProfessor;
    
}
