package classes;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Conta {

    private int numConta;
    private double saldo;
    private String nomeCliente, CPF, telefone, cidade, estado;

}
