package classes;

import java.util.ArrayList;
import utils.Util;

public class Metodos_Contas {

    public static void salvar(Conta conta) {
        Util.listaContas.add(conta);
    }
    
    public static void alterar(int index, Conta conta) {
        Util.listaContas.set(index, conta);
    }

    public static void excluir(int numConta) {
        Util.listaContas.removeIf(conta -> conta.getNumConta() == numConta);
    }

    public static ArrayList<Conta> buscar(String pesquisa) {
        ArrayList<Conta> contas = new ArrayList<>();

        Util.listaContas.forEach(conta -> {
            if (conta.getNomeCliente().equalsIgnoreCase(pesquisa)) {
                contas.add(conta);
            }
        });

        return contas;
    }

    public static boolean saque(int numConta, double valor) {
        int index = 0;
        double novoSaldo = 0;

        if (valor <= 0) {
            return false;
        }

        for (Conta conta : Util.listaContas) {
            if (conta.getNumConta() == numConta) {

                novoSaldo = (conta.getSaldo() - valor);

                if (novoSaldo < 0) {
                    return false;
                }

                Util.listaContas.get(index).setSaldo(novoSaldo);
                break;
            }
            index++;
        }

        return true;
    }

    public static boolean deposito(int numConta, double valor) {
        int index = 0;
        double novoSaldo = 0;

        if (valor <= 0) {
            return false;
        }

        for (Conta conta : Util.listaContas) {
            if (conta.getNumConta() == numConta) {

                novoSaldo = (conta.getSaldo() + valor);

                Util.listaContas.get(index).setSaldo(novoSaldo);
                break;
            }
            index++;
        }

        return true;
    }
}
