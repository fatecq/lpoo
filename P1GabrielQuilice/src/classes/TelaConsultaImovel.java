package classes;

import utils.Metodos;
import java.text.DecimalFormat;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import utils.Util;

public class TelaConsultaImovel extends javax.swing.JFrame {

    private TelaAlterarImovel telaAlterarImovel;

    private static DefaultTableModel modelo = new DefaultTableModel();
    private static DecimalFormat decimalFormat = new DecimalFormat("0.00");

    public TelaConsultaImovel() {
        initComponents();

        modelo = (DefaultTableModel) tableConsultaImovel.getModel();
        telaAlterarImovel = new TelaAlterarImovel();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tableConsultaImovel = new javax.swing.JTable();
        btnBuscar = new javax.swing.JButton();
        btnLimpar = new javax.swing.JButton();
        tfBuscaImovel = new javax.swing.JTextField();
        lbConsulta = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Consultar imóveis");
        setIconImage(Util.setProjectIconImage(this.getClass()));
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        tableConsultaImovel.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null}
            },
            new String [] {
                "Nº do imóvel", "Tipo", "Aluguel base", "Descrição", "Bairro", "Status"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableConsultaImovel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableConsultaImovelMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tableConsultaImovel);

        btnBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/buscar.png"))); // NOI18N
        btnBuscar.setText("Buscar");
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });

        btnLimpar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/limpar.png"))); // NOI18N
        btnLimpar.setText("Limpar pesquisa");
        btnLimpar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimparActionPerformed(evt);
            }
        });

        lbConsulta.setText("Pesquisar:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lbConsulta)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tfBuscaImovel, javax.swing.GroupLayout.PREFERRED_SIZE, 298, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnLimpar, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnLimpar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnBuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(tfBuscaImovel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lbConsulta)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 27, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 283, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed
        try {
            if (!tfBuscaImovel.getText().isEmpty()) {
                String textoPesquisa = tfBuscaImovel.getText();

                if (!loadImoveisbyFiltro(textoPesquisa)) {
                    Util.abreDialogAtencao("Imóveis", "Nenhum resultado correspondente para a pesquisa");
                    tfBuscaImovel.requestFocus();
                    tfBuscaImovel.selectAll();
                }

            } else {
                loadImoveis();
            }

            tfBuscaImovel.requestFocus();
        } catch (Exception e) {
            Util.abreDialogErro("Erro", e.getMessage());
        }
    }//GEN-LAST:event_btnBuscarActionPerformed

    private void btnLimparActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimparActionPerformed
        if (!tfBuscaImovel.getText().isEmpty()) {
            tfBuscaImovel.setText(null);
        }
        loadImoveis();
        tfBuscaImovel.requestFocus();
    }//GEN-LAST:event_btnLimparActionPerformed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        loadImoveis();
    }//GEN-LAST:event_formWindowOpened

    private void tableConsultaImovelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableConsultaImovelMouseClicked
        int linhaSelecionada = tableConsultaImovel.getSelectedRow();
        
        if (modelo.getValueAt(linhaSelecionada, 5).equals("Disponível")) {
            int escolha = JOptionPane.showConfirmDialog(null, "Deseja alterar os dados deste imóvel?", "Alterar",
                    JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

            if (escolha == JOptionPane.YES_OPTION) {
                abreTelaAlterarImovel((int) modelo.getValueAt(linhaSelecionada, 0));
            }
            
        } else {
            Util.abreDialogAtencao("Atenção", "Este imóvel está alugado, não é possível alterá-lo!");
        }
    }//GEN-LAST:event_tableConsultaImovelMouseClicked

    public void abreTelaAlterarImovel(int codigoImovel) {
        if (!telaAlterarImovel.isVisible()) {
            telaAlterarImovel = new TelaAlterarImovel(codigoImovel);
            telaAlterarImovel.setVisible(true);
        }
    }

    public static void loadImoveis() {
        try {
            modelo.setRowCount(0);

            Util.listaTipoImovel.forEach(tipoImovel -> {
                String tipo = "";
                String status = "";

                switch (tipoImovel.getTipo()) {
                    case 1:
                        tipo = "Residencial";
                        break;
                    case 2:
                        tipo = "Comercial";
                        break;
                    case 3:
                        tipo = "Galpão";
                        break;
                }

                if (tipoImovel.getStatus() == 'A') {
                    status = "Alugado";
                } else {
                    status = "Disponível";
                }

                modelo.addRow(new Object[]{
                    tipoImovel.getCodigoImovel(),
                    tipo,
                    "R$ " + decimalFormat.format(tipoImovel.getAluguelBase()),
                    tipoImovel.getDescricao(),
                    tipoImovel.getBairro(),
                    status,});
            });
        } catch (Exception e) {
            Util.abreDialogErro("Erro", e.getMessage());
        }
    }

    private boolean loadImoveisbyFiltro(String filtro) {
        modelo.setRowCount(0);

        Metodos metodos = new Metodos();

        for (Tipo_Imovel tipoImovel : metodos.buscar(filtro)) {
            String tipo = "";
            String status = "";

            switch (tipoImovel.getTipo()) {
                case 1:
                    tipo = "Residencial";
                    break;
                case 2:
                    tipo = "Comercial";
                    break;
                case 3:
                    tipo = "Galpão";
                    break;
            }

            if (tipoImovel.getStatus() == 'A') {
                status = "Alugado";
            } else {
                status = "Disponível";
            }

            modelo.addRow(new Object[]{
                tipoImovel.getCodigoImovel(),
                tipo,
                "R$ " + decimalFormat.format(tipoImovel.getAluguelBase()),
                tipoImovel.getDescricao(),
                tipoImovel.getBairro(),
                status,});
        }

        return modelo.getRowCount() != 0;
    }

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TelaConsultaImovel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TelaConsultaImovel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TelaConsultaImovel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaConsultaImovel.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TelaConsultaImovel().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnLimpar;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lbConsulta;
    private javax.swing.JTable tableConsultaImovel;
    private javax.swing.JTextField tfBuscaImovel;
    // End of variables declaration//GEN-END:variables
}
