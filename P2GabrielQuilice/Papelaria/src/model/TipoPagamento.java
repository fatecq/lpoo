package model;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class TipoPagamento {
    private int idTipo;
    private String descricao;
}
