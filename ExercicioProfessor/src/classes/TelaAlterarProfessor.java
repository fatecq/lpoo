package classes;

import javax.swing.ImageIcon;
import utils.Util;

public class TelaAlterarProfessor extends javax.swing.JFrame {
    private static int numRegistro;
    private int index = -1;
    private Professor professorSelecionado = new Professor();
    
    public TelaAlterarProfessor(int numRegistro) {
        this.numRegistro = numRegistro;
        initComponents();
        carregarProfessor();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lbNome = new javax.swing.JLabel();
        tfNomeProfessor = new javax.swing.JTextField();
        lbRegistro = new javax.swing.JLabel();
        tfNumRegistro = new javax.swing.JTextField();
        lbTitulacao = new javax.swing.JLabel();
        lbTotalHoraAula = new javax.swing.JLabel();
        tfTotalHoraAula = new javax.swing.JTextField();
        btnFechar = new javax.swing.JButton();
        btnSalvar = new javax.swing.JButton();
        comboBoxTitulacao = new javax.swing.JComboBox<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Alterar professor");
        setIconImage(new ImageIcon(getClass().getResource("/imagens/professor.png")).getImage());
        setResizable(false);

        lbNome.setText("Nome:");

        lbRegistro.setText("Nº do registro:");

        tfNumRegistro.setEnabled(false);

        lbTitulacao.setText("Titulação:");

        lbTotalHoraAula.setText("Valor total hora/aula semanal:");

        btnFechar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/sair.png"))); // NOI18N
        btnFechar.setText("FECHAR");
        btnFechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFecharActionPerformed(evt);
            }
        });

        btnSalvar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/salvar.png"))); // NOI18N
        btnSalvar.setText("SALVAR");
        btnSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarActionPerformed(evt);
            }
        });

        comboBoxTitulacao.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Escolha...", "Mestre", "Doutor", "Especialista", "Graduado" }));
        comboBoxTitulacao.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                comboBoxTitulacaoItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lbTotalHoraAula)
                            .addComponent(lbRegistro)
                            .addComponent(lbNome)
                            .addComponent(lbTitulacao))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(tfNumRegistro, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(tfTotalHoraAula, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(tfNomeProfessor, javax.swing.GroupLayout.PREFERRED_SIZE, 330, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(comboBoxTitulacao, javax.swing.GroupLayout.PREFERRED_SIZE, 217, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(108, 108, 108)
                        .addComponent(btnFechar, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(50, 50, 50)
                        .addComponent(btnSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(20, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbRegistro)
                    .addComponent(tfNumRegistro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbNome)
                    .addComponent(tfNomeProfessor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbTitulacao)
                    .addComponent(comboBoxTitulacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbTotalHoraAula, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tfTotalHoraAula, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnFechar, javax.swing.GroupLayout.DEFAULT_SIZE, 47, Short.MAX_VALUE)
                    .addComponent(btnSalvar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(24, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    
    private void carregarProfessor() {
        for (Professor professor : Util.listaProfessores) {
            if (professor.getNumRegistro() == numRegistro) {
                index = Util.listaProfessores.indexOf(professor);
                break;
            }
        }
        
        if(index == -1) {
            Util.abreDialogAtencao("Atenção", "Não foi encontrado nenhum professor com esse número de registro!");
            dispose();
        } else {
            professorSelecionado = Util.listaProfessores.get(index);
            
            tfNumRegistro.setText(String.valueOf(professorSelecionado.getNumRegistro()));
            tfNomeProfessor.setText(professorSelecionado.getNomeProfessor());
            
            switch (professorSelecionado.getTitulacao()) {
                case 1:
                    comboBoxTitulacao.setSelectedIndex(1);
                    break;
                case 2:
                    comboBoxTitulacao.setSelectedIndex(2);
                    break;
                case 3:
                    comboBoxTitulacao.setSelectedIndex(3);
                    break;
                case 4:
                    comboBoxTitulacao.setSelectedIndex(4);
                    break;
            }
            
            tfTotalHoraAula.setText(String.valueOf(professorSelecionado.getTotalHoraAula()));
        }
        
    }
    
    private void btnFecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFecharActionPerformed
        dispose();
    }//GEN-LAST:event_btnFecharActionPerformed

    private void btnSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarActionPerformed
        try {
            
            if (comboBoxTitulacao.getSelectedIndex() == 0){
                Util.abreDialogAtencao("Atenção", "Selecione a titulação do professor!");
                return;
            }
            
            professorSelecionado.setNumRegistro(numRegistro);
            professorSelecionado.setNomeProfessor(tfNomeProfessor.getText());
            professorSelecionado.setTitulacao(comboBoxTitulacao.getSelectedIndex());
            
            double valorHoraAula = 0;
            
            switch (comboBoxTitulacao.getSelectedIndex()) {
                case 1:
                    valorHoraAula = 15.50;
                    break;
                case 2:
                    valorHoraAula = 18.50;
                    break;
                case 3:
                    valorHoraAula = 13.50;
                    break;
                case 4:
                    valorHoraAula = 10.00;
                    break;
            }
            
            professorSelecionado.setValorHoraAula(valorHoraAula);
            professorSelecionado.setTotalHoraAula(Double.parseDouble( tfTotalHoraAula.getText() ));
        
            Util.listaProfessores.set(index, professorSelecionado);
            
            TelaGerenciarProfessor.loadProfessores();
            dispose();
        } catch (Exception e) {
            Util.abreDialogErro("Erro", e.getMessage());
        }
    }//GEN-LAST:event_btnSalvarActionPerformed

    private void comboBoxTitulacaoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_comboBoxTitulacaoItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_comboBoxTitulacaoItemStateChanged

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TelaAlterarProfessor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TelaAlterarProfessor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TelaAlterarProfessor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaAlterarProfessor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TelaAlterarProfessor(numRegistro).setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnFechar;
    private javax.swing.JButton btnSalvar;
    private javax.swing.JComboBox<String> comboBoxTitulacao;
    private javax.swing.JLabel lbNome;
    private javax.swing.JLabel lbRegistro;
    private javax.swing.JLabel lbTitulacao;
    private javax.swing.JLabel lbTotalHoraAula;
    private javax.swing.JTextField tfNomeProfessor;
    private javax.swing.JTextField tfNumRegistro;
    private javax.swing.JTextField tfTotalHoraAula;
    // End of variables declaration//GEN-END:variables
}
