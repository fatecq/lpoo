package classes;

import javax.swing.ImageIcon;
import utils.Util;

public class TelaPrincipal extends javax.swing.JFrame {
    
    public TelaPrincipal() {
        initComponents();
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnSair = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        menuCadProfessor = new javax.swing.JMenuItem();
        menuAlterarProfessor = new javax.swing.JMenuItem();
        menuExcluirProfessor = new javax.swing.JMenuItem();
        jMenu4 = new javax.swing.JMenu();
        menuConsultaNome = new javax.swing.JMenuItem();
        menuConsultaNumRegistro = new javax.swing.JMenuItem();
        menuAjuda = new javax.swing.JMenu();
        menuSobre = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        menuSair = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Exercício - Professor");
        setIconImage(new ImageIcon(getClass().getResource("/imagens/professor.png")).getImage());
        setResizable(false);

        btnSair.setText("SAIR");
        btnSair.setFocusPainted(false);
        btnSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSairActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Cantarell", 3, 24)); // NOI18N
        jLabel1.setText("Professores");

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/professor.png"))); // NOI18N

        jMenu1.setText("Professor");

        menuCadProfessor.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/novo.png"))); // NOI18N
        menuCadProfessor.setText("Cadastrar professor");
        menuCadProfessor.setPreferredSize(new java.awt.Dimension(181, 30));
        menuCadProfessor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuCadProfessorActionPerformed(evt);
            }
        });
        jMenu1.add(menuCadProfessor);

        menuAlterarProfessor.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/alterar.png"))); // NOI18N
        menuAlterarProfessor.setText("Alterar dados");
        menuAlterarProfessor.setPreferredSize(new java.awt.Dimension(181, 30));
        menuAlterarProfessor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuAlterarProfessorActionPerformed(evt);
            }
        });
        jMenu1.add(menuAlterarProfessor);

        menuExcluirProfessor.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/lixo.png"))); // NOI18N
        menuExcluirProfessor.setText("Excluir professor");
        menuExcluirProfessor.setPreferredSize(new java.awt.Dimension(150, 30));
        menuExcluirProfessor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuExcluirProfessorActionPerformed(evt);
            }
        });
        jMenu1.add(menuExcluirProfessor);

        jMenuBar1.add(jMenu1);

        jMenu4.setText("Consultas");

        menuConsultaNome.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/monitorar.png"))); // NOI18N
        menuConsultaNome.setText("Consultar professor por nome");
        menuConsultaNome.setPreferredSize(new java.awt.Dimension(227, 30));
        menuConsultaNome.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuConsultaNomeActionPerformed(evt);
            }
        });
        jMenu4.add(menuConsultaNome);

        menuConsultaNumRegistro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/buscar.png"))); // NOI18N
        menuConsultaNumRegistro.setText("Consultar por nº do registro");
        menuConsultaNumRegistro.setPreferredSize(new java.awt.Dimension(227, 30));
        menuConsultaNumRegistro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuConsultaNumRegistroActionPerformed(evt);
            }
        });
        jMenu4.add(menuConsultaNumRegistro);

        jMenuBar1.add(jMenu4);

        menuAjuda.setText("Ajuda");

        menuSobre.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/versao.png"))); // NOI18N
        menuSobre.setText("Sobre");
        menuSobre.setPreferredSize(new java.awt.Dimension(90, 30));
        menuSobre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSobreActionPerformed(evt);
            }
        });
        menuAjuda.add(menuSobre);
        menuAjuda.add(jSeparator1);

        menuSair.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/fechar.png"))); // NOI18N
        menuSair.setText("Sair");
        menuSair.setPreferredSize(new java.awt.Dimension(90, 30));
        menuSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSairActionPerformed(evt);
            }
        });
        menuAjuda.add(menuSair);

        jMenuBar1.add(menuAjuda);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(141, 141, 141)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(btnSair, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(146, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel1)
                .addGap(39, 39, 39)
                .addComponent(btnSair, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(28, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSairActionPerformed
        System.exit(0);
    }//GEN-LAST:event_btnSairActionPerformed

    private void menuCadProfessorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuCadProfessorActionPerformed
        TelaCadastroProfessor telaCadProfessor = new TelaCadastroProfessor();
        telaCadProfessor.setVisible(true);
    }//GEN-LAST:event_menuCadProfessorActionPerformed

    private void menuSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSairActionPerformed
        System.exit(0);
    }//GEN-LAST:event_menuSairActionPerformed

    private void menuSobreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuSobreActionPerformed
        Util.abreDialogInformacao("Sobre", "Exercício de LPOO - Gabriel Quilice");
    }//GEN-LAST:event_menuSobreActionPerformed

    private void menuConsultaNomeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuConsultaNomeActionPerformed
        TelaConsultaProfessor telaConsultaProfessor = new TelaConsultaProfessor(Util.CONSULTA_NOME);
        telaConsultaProfessor.setVisible(true);
    }//GEN-LAST:event_menuConsultaNomeActionPerformed

    private void menuConsultaNumRegistroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuConsultaNumRegistroActionPerformed
        TelaConsultaProfessor telaConsultaProfessor = new TelaConsultaProfessor(Util.CONSULTA_NUM_REG);
        telaConsultaProfessor.setVisible(true);
    }//GEN-LAST:event_menuConsultaNumRegistroActionPerformed

    private void menuAlterarProfessorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuAlterarProfessorActionPerformed
        TelaGerenciarProfessor telaGerenciarProfessor = new TelaGerenciarProfessor(Util.ALTERAR_PROFESSOR);
        telaGerenciarProfessor.setVisible(true);
    }//GEN-LAST:event_menuAlterarProfessorActionPerformed

    private void menuExcluirProfessorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuExcluirProfessorActionPerformed
        TelaGerenciarProfessor telaGerenciarProfessor = new TelaGerenciarProfessor(Util.EXCLUIR_PROFESSOR);
        telaGerenciarProfessor.setVisible(true);
    }//GEN-LAST:event_menuExcluirProfessorActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TelaPrincipal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnSair;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JMenu menuAjuda;
    private javax.swing.JMenuItem menuAlterarProfessor;
    private javax.swing.JMenuItem menuCadProfessor;
    private javax.swing.JMenuItem menuConsultaNome;
    private javax.swing.JMenuItem menuConsultaNumRegistro;
    private javax.swing.JMenuItem menuExcluirProfessor;
    private javax.swing.JMenuItem menuSair;
    private javax.swing.JMenuItem menuSobre;
    // End of variables declaration//GEN-END:variables
}
