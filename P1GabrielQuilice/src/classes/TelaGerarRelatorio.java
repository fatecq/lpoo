package classes;

import java.text.DecimalFormat;
import javax.swing.table.DefaultTableModel;
import utils.Util;

public class TelaGerarRelatorio extends javax.swing.JFrame {

    private static DefaultTableModel modeloAlugados = new DefaultTableModel();
    private static DefaultTableModel modeloDisponiveis = new DefaultTableModel();
    private static DecimalFormat decimalFormat = new DecimalFormat("0.00");

    public TelaGerarRelatorio() {
        initComponents();

        modeloAlugados = (DefaultTableModel) tableImoveisAlugados.getModel();
        modeloDisponiveis = (DefaultTableModel) tableImoveisDisponiveis.getModel();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnFechar = new javax.swing.JButton();
        pnImoveisAlugados = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableImoveisAlugados = new javax.swing.JTable();
        pnImoveisDiponiveis = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tableImoveisDisponiveis = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        tfTotalImoveis = new javax.swing.JTextField();
        lbTotalImoveis = new javax.swing.JLabel();
        lbTotalAlugados = new javax.swing.JLabel();
        lbTotalDisponiveis = new javax.swing.JLabel();
        tfTotalAlugados = new javax.swing.JTextField();
        tfTotalDisponiveis = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Relatório");
        setIconImage(Util.setProjectIconImage(this.getClass()));
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        btnFechar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/sair.png"))); // NOI18N
        btnFechar.setText("FECHAR");
        btnFechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFecharActionPerformed(evt);
            }
        });

        pnImoveisAlugados.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Imóveis alugados"));

        tableImoveisAlugados.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null}
            },
            new String [] {
                "Nº do imóvel", "Tipo", "Aluguel base", "Descrição", "Bairro", "Valor Final"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tableImoveisAlugados);

        javax.swing.GroupLayout pnImoveisAlugadosLayout = new javax.swing.GroupLayout(pnImoveisAlugados);
        pnImoveisAlugados.setLayout(pnImoveisAlugadosLayout);
        pnImoveisAlugadosLayout.setHorizontalGroup(
            pnImoveisAlugadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnImoveisAlugadosLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1)
                .addContainerGap())
        );
        pnImoveisAlugadosLayout.setVerticalGroup(
            pnImoveisAlugadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnImoveisAlugadosLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 198, Short.MAX_VALUE)
                .addContainerGap())
        );

        pnImoveisDiponiveis.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Imóveis disponíveis"));

        tableImoveisDisponiveis.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null}
            },
            new String [] {
                "Nº do imóvel", "Tipo", "Aluguel base", "Descrição", "Bairro"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tableImoveisDisponiveis.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableImoveisDisponiveisMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tableImoveisDisponiveis);

        javax.swing.GroupLayout pnImoveisDiponiveisLayout = new javax.swing.GroupLayout(pnImoveisDiponiveis);
        pnImoveisDiponiveis.setLayout(pnImoveisDiponiveisLayout);
        pnImoveisDiponiveisLayout.setHorizontalGroup(
            pnImoveisDiponiveisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnImoveisDiponiveisLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 728, Short.MAX_VALUE)
                .addContainerGap())
        );
        pnImoveisDiponiveisLayout.setVerticalGroup(
            pnImoveisDiponiveisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnImoveisDiponiveisLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 198, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)), "Informações"));

        tfTotalImoveis.setEnabled(false);

        lbTotalImoveis.setText("Total de imóveis:");

        lbTotalAlugados.setText("Total de imóveis alugados:");

        lbTotalDisponiveis.setText("Total de imóveis disponíveis:");

        tfTotalAlugados.setEnabled(false);

        tfTotalDisponiveis.setEnabled(false);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lbTotalAlugados)
                    .addComponent(lbTotalImoveis))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(tfTotalAlugados, javax.swing.GroupLayout.DEFAULT_SIZE, 128, Short.MAX_VALUE)
                    .addComponent(tfTotalImoveis))
                .addGap(39, 39, 39)
                .addComponent(lbTotalDisponiveis)
                .addGap(18, 18, 18)
                .addComponent(tfTotalDisponiveis, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfTotalImoveis, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbTotalImoveis))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbTotalDisponiveis)
                    .addComponent(lbTotalAlugados)
                    .addComponent(tfTotalAlugados, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tfTotalDisponiveis, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(15, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(308, 308, 308)
                        .addComponent(btnFechar, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(pnImoveisAlugados, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(pnImoveisDiponiveis, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnImoveisAlugados, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(pnImoveisDiponiveis, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnFechar, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnFecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFecharActionPerformed
        dispose();
    }//GEN-LAST:event_btnFecharActionPerformed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        loadImoveis();
    }//GEN-LAST:event_formWindowOpened

    private void tableImoveisDisponiveisMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableImoveisDisponiveisMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_tableImoveisDisponiveisMouseClicked

    private void loadImoveis() {
        try {
            modeloAlugados.setRowCount(0);
            modeloDisponiveis.setRowCount(0);

            Util.listaTipoImovel.forEach(tipoImovel -> {
                String tipo = "";

                switch (tipoImovel.getTipo()) {
                    case 1:
                        tipo = "Residencial";
                        break;
                    case 2:
                        tipo = "Comercial";
                        break;
                    case 3:
                        tipo = "Galpão";
                        break;
                }

                if (tipoImovel.getStatus() == 'A') {
                    modeloAlugados.addRow(new Object[]{
                        tipoImovel.getCodigoImovel(),
                        tipo,
                        "R$ " + decimalFormat.format(tipoImovel.getAluguelBase()),
                        tipoImovel.getDescricao(),
                        tipoImovel.getBairro(),
                        "R$ " + decimalFormat.format(tipoImovel.getValorFinal()),});

                } else {
                    modeloDisponiveis.addRow(new Object[]{
                        tipoImovel.getCodigoImovel(),
                        tipo,
                        "R$ " + decimalFormat.format(tipoImovel.getAluguelBase()),
                        tipoImovel.getDescricao(),
                        tipoImovel.getBairro(),});
                }

            });

            tfTotalImoveis.setText(String.valueOf(Util.listaTipoImovel.size()));
            tfTotalAlugados.setText(String.valueOf(modeloAlugados.getRowCount()));
            tfTotalDisponiveis.setText(String.valueOf(modeloDisponiveis.getRowCount()));

        } catch (Exception e) {
            Util.abreDialogErro("Erro", e.getMessage());
        }
    }

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TelaGerarRelatorio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TelaGerarRelatorio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TelaGerarRelatorio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaGerarRelatorio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TelaGerarRelatorio().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnFechar;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lbTotalAlugados;
    private javax.swing.JLabel lbTotalDisponiveis;
    private javax.swing.JLabel lbTotalImoveis;
    private javax.swing.JPanel pnImoveisAlugados;
    private javax.swing.JPanel pnImoveisDiponiveis;
    private javax.swing.JTable tableImoveisAlugados;
    private javax.swing.JTable tableImoveisDisponiveis;
    private javax.swing.JTextField tfTotalAlugados;
    private javax.swing.JTextField tfTotalDisponiveis;
    private javax.swing.JTextField tfTotalImoveis;
    // End of variables declaration//GEN-END:variables
}
