package classes;

import java.text.DecimalFormat;
import utils.Constantes;
import utils.Metodos;
import utils.Util;

public class TelaOperacoes extends javax.swing.JFrame {

    private Metodos metodos;
    private static int tipoOperacao;
    private DecimalFormat decimalFormat = new DecimalFormat("0.00");

    public TelaOperacoes(int tipoOperacao) {
        this.tipoOperacao = tipoOperacao;
        this.metodos = new Metodos();

        initComponents();

        if (tipoOperacao == Constantes.OPERACAO_ALUGAR) {
            setTitle("Realizar alocação");
            btnOperacao.setText("ALUGAR");
        } else {
            setTitle("Realizar devolução");
            btnOperacao.setText("DEVOLVER");
        }
    }

    public TelaOperacoes() {}

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnFechar = new javax.swing.JButton();
        btnOperacao = new javax.swing.JButton();
        lbCodigoImovel = new javax.swing.JLabel();
        tfCodigoImovel = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Alocação");
        setIconImage(Util.setProjectIconImage(this.getClass())
        );
        setResizable(false);

        btnFechar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/sair.png"))); // NOI18N
        btnFechar.setText("FECHAR");
        btnFechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFecharActionPerformed(evt);
            }
        });

        btnOperacao.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/confirmar.png"))); // NOI18N
        btnOperacao.setText("ALUGAR");
        btnOperacao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOperacaoActionPerformed(evt);
            }
        });

        lbCodigoImovel.setText("Nº do imóvel:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnFechar, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(42, 42, 42)
                        .addComponent(btnOperacao, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lbCodigoImovel)
                        .addGap(18, 18, 18)
                        .addComponent(tfCodigoImovel)))
                .addContainerGap(22, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbCodigoImovel)
                    .addComponent(tfCodigoImovel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnOperacao, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnFechar, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(27, 27, 27))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnOperacaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOperacaoActionPerformed
        try {
            int codigoImovel = Integer.parseInt(tfCodigoImovel.getText());

            if (!validaImovel(codigoImovel)) {
                Util.abreDialogAtencao("Atenção", "Não existe nenhum imóvel com o código " + codigoImovel + ", favor verifique!");
                reiniciaProcesso();
                return;
            }

            if (tipoOperacao == Constantes.OPERACAO_ALUGAR) {

                if (!metodos.alocacao(codigoImovel)) {
                    Util.abreDialogAtencao("Atenção", "Não foi possível alugar, o imóvel está indisponível!");
                } else {
                    Util.abreDialogInformacao("Alocação", "Imóvel alugado com sucesso!\n\n"
                            + "Valor final do aluguel: R$" + decimalFormat.format(metodos.getTipoImovelValorFinal(codigoImovel)));
                    reiniciaProcesso();
                }

            } else {

                if (!metodos.devolucao(codigoImovel)) {
                    Util.abreDialogAtencao("Atenção", "Não foi possível devolver o imóvel, favor verifique!");
                } else {
                    Util.abreDialogInformacao("Devolução", "Imóvel devolvido com sucesso!");
                    reiniciaProcesso();
                }

            }
        } catch (Exception e) {
            Util.abreDialogErro("Erro", e.getMessage());
        }
    }//GEN-LAST:event_btnOperacaoActionPerformed

    private void btnFecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFecharActionPerformed
        dispose();
    }//GEN-LAST:event_btnFecharActionPerformed

    private boolean validaImovel(int codigoImovel) {
        return Util.listaTipoImovel.stream().anyMatch(tipoImovel -> tipoImovel.getCodigoImovel()== codigoImovel);
    }

    private void reiniciaProcesso() {
        tfCodigoImovel.setText(null);
        tfCodigoImovel.requestFocus();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TelaOperacoes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TelaOperacoes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TelaOperacoes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaOperacoes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TelaOperacoes(tipoOperacao).setVisible(true);
            }
        });
        
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnFechar;
    private javax.swing.JButton btnOperacao;
    private javax.swing.JLabel lbCodigoImovel;
    private javax.swing.JTextField tfCodigoImovel;
    // End of variables declaration//GEN-END:variables
}
