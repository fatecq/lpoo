package classes;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Tipo_Imovel extends Imovel {
    private String descricao;
    private String bairro;
    private char status;
    private double valorFinal;
}
