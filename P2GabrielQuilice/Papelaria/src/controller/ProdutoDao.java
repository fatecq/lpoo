package controller;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import model.Produto;

public class ProdutoDao {

    private final ConexaoDao conexaoDao;

    public ProdutoDao() {
        this.conexaoDao = new ConexaoDao();
    }

    public boolean cadastrar(Produto produto) throws Exception {
        try {
            conexaoDao.conectar();

            if (!produto.getCodigoBarras().equals("")) {
                PreparedStatement stmt = conexaoDao.getConnection().prepareStatement("SELECT * FROM produto "
                        + "WHERE codigo_barras = ?");
                stmt.setString(1, produto.getCodigoBarras());
                if (stmt.executeQuery().next()) {
                    return false;
                }
            }

            PreparedStatement stmt = conexaoDao.getConnection().prepareStatement(
                    "INSERT INTO produto(nome, codigo_barras, qt_estoque, preco, fg_ativo) VALUES "
                    + "(?, ?, ?, ?, 'S')");
            stmt.setString(1, produto.getDescricao());
            stmt.setString(2, produto.getCodigoBarras());
            stmt.setInt(3, produto.getQtEstoque());
            stmt.setDouble(4, produto.getPreco());

            stmt.execute();
            conexaoDao.getConnection().commit();

            conexaoDao.desconectar();

            return true;
        } catch (Exception e) {
            conexaoDao.desconectar();
            throw e;
        }
    }

    public boolean alterar(Produto produto) throws Exception {
        try {
            conexaoDao.conectar();

            if (!produto.getCodigoBarras().equals("")) {
                PreparedStatement stmt = conexaoDao.getConnection().prepareStatement("SELECT * FROM produto "
                        + "WHERE codigo_barras = ? AND idproduto <> ?");
                stmt.setString(1, produto.getCodigoBarras());
                stmt.setInt(2, produto.getIdProduto());
                if (stmt.executeQuery().next()) {
                    return false;
                }
            }

            PreparedStatement stmt = conexaoDao.getConnection().prepareStatement(
                    "UPDATE produto SET nome = ?, codigo_barras = ?, preco = ? WHERE idproduto = ?");
            stmt.setString(1, produto.getDescricao());
            stmt.setString(2, produto.getCodigoBarras());
            stmt.setDouble(3, produto.getPreco());
            stmt.setInt(4, produto.getIdProduto());

            stmt.execute();
            conexaoDao.getConnection().commit();

            conexaoDao.desconectar();

            return true;
        } catch (Exception e) {
            conexaoDao.desconectar();
            throw e;
        }
    }

    public boolean excluir(int idProduto) throws Exception {
        try {
            conexaoDao.conectar();

            PreparedStatement stmt = conexaoDao.getConnection().prepareStatement(
                    "DELETE FROM produto WHERE idproduto = ?");
            stmt.setInt(1, idProduto);

            stmt.execute();
            conexaoDao.getConnection().commit();

            conexaoDao.desconectar();

            return true;
        } catch (Exception e) {
            conexaoDao.desconectar();
            throw e;
        }
    }

    public Produto procurarProduto(String codigo, boolean verificarEstoque) throws Exception {
        try {
            Produto produto = new Produto();
            conexaoDao.conectar();

            PreparedStatement stmt = conexaoDao.getConnection().prepareStatement("SELECT * FROM produto "
                    + "WHERE (codigo_barras = ? OR idproduto = ?)"
                    + (verificarEstoque ? " AND qt_estoque <> 0 " : " ") + "AND fg_ativo = 'S'");
            stmt.setString(1, codigo);
            stmt.setInt(2, Integer.valueOf(codigo));

            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                produto.setIdProduto(result.getInt("idproduto"));
                produto.setDescricao(result.getString("nome"));
                produto.setCodigoBarras(result.getString("codigo_barras"));
                produto.setQtEstoque(result.getInt("qt_estoque"));
                produto.setPreco(result.getDouble("preco"));

                return produto;
            } else {
                return null;
            }

        } catch (Exception e) {
            conexaoDao.desconectar();
            throw e;
        }
    }

    public ArrayList<Produto> getListaProdutos() throws Exception {
        try {
            ArrayList<Produto> listaProdutos = new ArrayList<>();
            conexaoDao.conectar();

            PreparedStatement stmt = conexaoDao.getConnection().prepareStatement("SELECT * FROM produto "
                    + "WHERE  fg_ativo = 'S' ORDER BY nome");

            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                Produto produto = new Produto();
                produto.setIdProduto(result.getInt("idproduto"));
                produto.setDescricao(result.getString("nome"));
                produto.setCodigoBarras(result.getString("codigo_barras"));
                produto.setQtEstoque(result.getInt("qt_estoque"));
                produto.setPreco(result.getDouble("preco"));
                listaProdutos.add(produto);
            }

            return listaProdutos;

        } catch (SQLException e) {
            conexaoDao.desconectar();
            throw e;
        }
    }

    public ArrayList<Produto> getListaProdutos(String descricao) throws Exception {
        try {
            ArrayList<Produto> listaProdutos = new ArrayList<>();
            conexaoDao.conectar();

            PreparedStatement stmt = conexaoDao.getConnection().prepareStatement("SELECT * FROM produto "
                    + "WHERE  nome LIKE ? AND fg_ativo = 'S' ORDER BY nome");
            stmt.setString(1, "%" + descricao + "%");

            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                Produto produto = new Produto();
                produto.setIdProduto(result.getInt("idproduto"));
                produto.setDescricao(result.getString("nome"));
                produto.setCodigoBarras(result.getString("codigo_barras"));
                produto.setQtEstoque(result.getInt("qt_estoque"));
                produto.setPreco(result.getDouble("preco"));
                listaProdutos.add(produto);
            }

            return listaProdutos;

        } catch (SQLException e) {
            conexaoDao.desconectar();
            throw e;
        }
    }

    public ArrayList<Produto> getListaProdutos(String codigo, int codigoInt) throws Exception {
        try {
            ArrayList<Produto> listaProdutos = new ArrayList<>();
            conexaoDao.conectar();

            PreparedStatement stmt = conexaoDao.getConnection().prepareStatement("SELECT * FROM produto "
                    + "WHERE (codigo_barras = ? OR idproduto = ?) AND fg_ativo = 'S' ORDER BY nome");
            stmt.setString(1, codigo);
            stmt.setInt(2, codigoInt);

            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                Produto produto = new Produto();
                produto.setIdProduto(result.getInt("idproduto"));
                produto.setDescricao(result.getString("nome"));
                produto.setCodigoBarras(result.getString("codigo_barras"));
                produto.setQtEstoque(result.getInt("qt_estoque"));
                produto.setPreco(result.getDouble("preco"));
                listaProdutos.add(produto);
            }

            return listaProdutos;

        } catch (SQLException e) {
            conexaoDao.desconectar();
            throw e;
        }
    }
}
